<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\LoginController;
use App\Http\Controllers\RegisterController;
use App\Http\Controllers\AdminDesaController;
use App\Http\Controllers\PerangkatController;
use App\Http\Controllers\EditorController;
use App\Http\Controllers\BlogController;
use App\Http\Controllers\ViewController;
use App\Http\Controllers\PeraturanController;
use App\Http\Controllers\PengumumanController;
use App\Http\Controllers\ProfilController;
use App\Models\Blog;
use App\Models\Peraturan;
use App\Models\Pengumuman;
use Illuminate\Support\Facades\DB;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    $blog = Blog::latest()->paginate(3);
    $peraturan = Peraturan::orderBy('id')->get();
    $pengumuman = Pengumuman::latest()->paginate(6);
    return view('beranda.utama', [
        'blogs' => $blog,
        'peraturans' => $peraturan,
        'pengumumans' => $pengumuman
    ]);
});

Route::get('/regulasi', function () {
    $peraturan = Peraturan::orderBy('id')->get();
    return view('beranda.regulasi', [
        'peraturans' => $peraturan
    ]);
});

//Profil Desa
Route::resource('profil', ProfilController::class);
// Route::get('/pilihdesa', [ProfilController::class, 'pilihdesa']);
Route::post('/profil/lihatprofil', [ProfilController::class, 'lihatprofil']);
Route::post('/getDesa', [ProfilController::class, 'getDesa']);

Route::get('/view/{id}', [ViewController::class, 'lihat']);
Route::get('/viewperaturan/{id}', [ViewController::class, 'lihatperaturan']);
Route::get('/viewpengumuman/{id}', [ViewController::class, 'lihatpengumuman']);


Route::post('/masuk', [LoginController::class, 'cekMasuk']);
Route::get('/login', function () {
    return view('beranda.login');
});

Route::group(['middleware' => ['AuthDesa']], function () {

    Route::get('/adminDesa', [AdminDesaController::class, 'index']);
    Route::post('/logoutDesa', [AdminDesaController::class, 'logout']);
    Route::get('/logoutDesa', [AdminDesaController::class, 'logout']);
    Route::get('/adminDesa/formKewilayahan', [AdminDesaController::class, 'formKewilayahan']);
    Route::post('/adminDesa/tambahDatumWil', [AdminDesaController::class, 'tambahDatumWil']);
    Route::post('/adminDesa/updateDatumWil', [AdminDesaController::class, 'updateDatumWil']);

    Route::get('/adminDesa/formPerangkat', [PerangkatController::class, 'formPerangkat']);
    Route::post('/adminDesa/tambahDatumPer', [PerangkatController::class, 'tambahDatumPer']);
    Route::post('/adminDesa/updateDatumPer', [PerangkatController::class, 'updateDatumPer']);
    Route::post('/adminDesa/copyDatumPer', [PerangkatController::class, 'copyDatumPer']);
});

Route::group(['middleware' => ['AuthEditor']], function () {
    Route::get('/editor', [EditorController::class, 'index']);
    Route::post('/logoutEditor', [EditorController::class, 'logoutEditor']);
    Route::get('/logoutEditor', [EditorController::class, 'logoutEditor']);
    Route::resource('blog', BlogController::class);
    Route::resource('peraturan', PeraturanController::class);
    Route::resource('pengumuman', PengumumanController::class);
});


Route::get('/register', [RegisterController::class, 'index']);
Route::get('/register/tambah', [RegisterController::class, 'formTambah']);
Route::post('/register/store', [RegisterController::class, 'store']);
Route::post('/register/delete/{data}', [RegisterController::class, 'delete']);
