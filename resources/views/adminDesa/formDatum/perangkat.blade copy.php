@extends('templates.desa.main')

@section('content')
<div class="container">
    <h3>FORM INPUT / UPDATE DATA PERANGKAT DESA</h3>

    <div class="col-md-12 col-sm-12  ">
        <div class="x_panel">
            <div class="x_title">
                <h2>Silahkan isi data kades dan perangkat dibawah ini, selengkap mungkin</h2>
                <ul class="nav navbar-right panel_toolbox">
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                    </li>
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button"
                            aria-expanded="false"><i class="fa fa-wrench"></i></a>

                    </li>
                    <li><a class="close-link"><i class="fa fa-close"></i></a>
                    </li>
                </ul>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">

                <ul class="nav nav-tabs bar_tabs" id="myTab" role="tablist">
                    <li class="nav-item">
                        <a class="nav-link bg-dark text-white" id="home-tab" data-toggle="tab" href="#home" role="tab"
                            aria-controls="home" aria-selected="true">Kades</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" id="profile-tab"
                            href="/adminDesa/formPerangkat?jabatan=Kepala Desa">Sekdes</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" id="contact-tab" data-toggle="tab" href="#contact" role="tab"
                            aria-controls="contact" aria-selected="false">Kaur Umum</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" id="contact-tab" data-toggle="tab" href="#contact" role="tab"
                            aria-controls="contact" aria-selected="false">Kaur Perencanaan</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" id="contact-tab" data-toggle="tab" href="#contact" role="tab"
                            aria-controls="contact" aria-selected="false">Kaur Keuangan</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" id="contact-tab" data-toggle="tab" href="#contact" role="tab"
                            aria-controls="contact" aria-selected="false">Kasi Pemerintahan</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" id="contact-tab" data-toggle="tab" href="#contact" role="tab"
                            aria-controls="contact" aria-selected="false">Kasi Kesra</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" id="contact-tab" data-toggle="tab" href="#contact" role="tab"
                            aria-controls="contact" aria-selected="false">Kasi Pelayanan</a>
                    </li>
                </ul>
                <div class="tab-content" id="myTabContent">
                    <div class="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">
                        <form action="/adminDesa/tambahDatumPer" method="post" enctype="multipart/form-data"
                            class="form-horizontal form-label-left">
                            @csrf

                            <div class="form-group row ">
                                <label class="control-label col-md-2 col-sm-2 ">Jabatan</label>
                                <div class="col-md-5 col-sm-5 ">
                                    <input type="text" name="jabatan" class="form-control" value="Kepala Desa"
                                        style="font-size: .85rem" readonly>
                                    <input type="hidden" name="asal_id" value="{{ session('loggedAdminDesa') }}">
                                    <input type="hidden" name="tahun" value="{{ now()->format('Y') }}">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="control-label col-md-2 col-sm-2 ">Nama </label>
                                <div class="col-md-5 col-sm-5 ">
                                    <input type="text" class="form-control" name="nama" style="font-size: .85rem"
                                        value="{{ old('nama') }}" required>
                                </div>
                                @error('nama')
                                <div><small class="text-danger">{{ $message }}</small></div>
                                @enderror
                            </div>
                            <div class="form-group row">
                                <label class="control-label col-md-2 col-sm-2 ">Tempat Lahir </label>
                                <div class="col-md-5 col-sm-5 ">
                                    <input type="text" class="form-control " name="tempat_lahir"
                                        style="font-size: .85rem" value="{{ old('tempat_lahir') }}" required>
                                </div>
                                @error('tempat_lahir')
                                <div><small class="text-danger">{{ $message }}</small></div>
                                @enderror
                            </div>
                            <div class="form-group row">
                                <label class="control-label col-md-2 col-sm-2 ">Tanggal Lahir</label>

                                <div class="col-md-5 col-sm-5 col-xs-9">
                                    <input type="text" name="tgl_lahir" class="form-control"
                                        data-inputmask="'mask': '99/99/9999'" value="{{ old('tgl_lahir') }}" required>
                                    <span class="fa fa-user form-control-feedback right" aria-hidden="true"></span>
                                </div>
                                @error('tgl_lahir')
                                <div><small class="text-danger">{{ $message }}</small></div>
                                @enderror

                            </div>
                            <div class="form-group row">
                                <label class="control-label col-md-2 col-sm-2 ">Jenis Kelamin</label>
                                <div class="col-md-5 col-sm-5 d-flex align-items-center">
                                    <span>
                                        <input type="radio" name="jenkel" value="L" checked="" />
                                        Laki-laki
                                        <input type="radio" class="ml-3" name="jenkel" value="P" />
                                        Perempuan

                                    </span>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="control-label col-md-2 col-sm-2 ">Agama </label>
                                <div class="col-md-5 col-sm-5">
                                    <input type="text" class="form-control" name="agama" value="{{ old('agama') }}"
                                        style="font-size: .85rem">
                                </div>
                                @error('agama')
                                <div><small class="text-danger">{{ $message }}</small></div>
                                @enderror
                            </div>
                            <div class="form-group row">
                                <label class="control-label col-md-2 col-sm-2 ">SK Jabatan </label>
                                <div class="col-md-5 col-sm-5 ">
                                    <span class="input-group-text" style="font-size: .85rem; border-radius: 0;">Nomor
                                        : <input type="text" class="form-control ml-2" name="nomor_sk"
                                            value="{{ old('nomor_sk') }}" style="font-size: .85rem" required></span>

                                </div>
                                @error('nomor_sk')
                                <div><small class="text-danger">{{ $message }}</small></div>
                                @enderror
                            </div>
                            <div class="form-group row">
                                <label class="control-label col-md-2 col-sm-2 ">Masa Berlaku SK Jabatan</label>
                                <div class="col-md-5 col-sm-5 ">
                                    <span class="input-group-text" style="font-size: .85rem; border-radius: 0;">sejak
                                        : <input type="text" name="sejak" class="form-control mx-2"
                                            data-inputmask="'mask': '99/99/9999'" value="{{ old('sejak') }}"
                                            required>s.d<input type="text" name="sampai" class="form-control mx-2"
                                            placeholder="tahun berakhir" value="{{ old('sampai') }}"></span>

                                </div>
                                @error('sejak')
                                <div><small class="text-danger">{{ $message }}</small></div>
                                @enderror
                            </div>

                            <div class="form-group row">
                                <label class="control-label col-md-2 col-sm-2 ">Pendidikan Terakhir</label>
                                <div class="col-md-5 col-sm-5 ">
                                    <select class="form-control" style="font-size: .85rem" name="pendidikan" required>
                                        <option value="">Pilih</option>
                                        <option value="SD/Sederajat">SD/Sederajat</option>
                                        <option value="SMP/Sederajat">SMP/Sederajat</option>
                                        <option value="SMA/Sederajat">SMA/Sederajat</option>
                                        <option value="Diploma">Diploma</option>
                                        <option value="Sarjana/S1">Sarjana/S1</option>
                                        <option value="Magister/S2">Magister/S2</option>
                                        <option value="Doktoral/S3">Doktoral/S3</option>

                                    </select>
                                </div>
                                @error('pendidikan')
                                <div><small class="text-danger">{{ $message }}</small></div>
                                @enderror
                            </div>
                            <div class="form-group row">
                                <label class="control-label col-md-2 col-sm-2 ">Upload Foto (jpg/png)</label>
                                <div class="col-md-2 col-sm-5">
                                    <p class="image_upload mb-0">
                                        <label for="foto_perangkat">
                                            <a class="btn btn-warning btn-sm" rel="nofollow"><span
                                                    class='fa fa-file'></span> Sisipkan foto</a>
                                        </label>
                                        <input type="file" name="foto_perangkat" id="foto_perangkat"
                                            style="display: none">
                                    </p>
                                    <div class="foto_p" style="display: none">
                                        <p class="nfile">nama file</p><img src="/img/logo.png" class="gb_perangkat"
                                            width="150">
                                    </div>
                                </div>

                            </div>


                            <div class="ln_solid"></div>
                            <div class="form-group">
                                <div class="col-md-5 col-sm-5  offset-md-2">
                                    <button type="button" class="btn btn-primary">Cancel</button>
                                    <button type="reset" class="btn btn-primary">Reset</button>
                                    <button type="submit" class="btn btn-success">Submit</button>
                                </div>
                            </div>

                        </form>
                    </div>
                    <div class="tab-pane fade" id="profile" role="tabpanel" aria-labelledby="profile-tab">
                        Food truck fixie locavore, accusamus mcsweeney's marfa nulla single-origin coffee squid.
                        Exercitation +1 labore velit, blog sartorial PBR leggings next level wes anderson artisan four
                        loko farm-to-table craft beer twee. Qui photo
                        booth letterpress, commodo enim craft beer mlkshk aliquip
                    </div>
                    <div class="tab-pane fade" id="contact" role="tabpanel" aria-labelledby="contact-tab">
                        xxFood truck fixie locavore, accusamus mcsweeney's marfa nulla single-origin coffee squid.
                        Exercitation +1 labore velit, blog sartorial PBR leggings next level wes anderson artisan four
                        loko farm-to-table craft beer twee. Qui photo
                        booth letterpress, commodo enim craft beer mlkshk
                    </div>
                </div>
                <br><br><br>
            </div>
        </div>
    </div>
    <br>
    <br>

</div>


@endsection
@push('script')
<!-- jquery.inputmask -->
<script src="/vendors/jquery.inputmask/dist/min/jquery.inputmask.bundle.min.js"></script>
<script>
    $("#file_sk").change(function(event) {

        getURL(this);
    });

function getURL(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();
        var filename = $("#file_sk").val();
        
        filename = filename.substring(filename.lastIndexOf('\\') + 1);
        var cekgb = filename.substring(filename.lastIndexOf('.') + 1);
        if (cekgb == 'pdf' || cekgb == 'PDF') {
            if(input.files[0]['size'] > 1024){
                alert('ukuran file tidak boleh > 1 Mb !');
                $('#file_sk').val("");
            }else{
                $('#nmfile_sk').html('');
                $('#nmfile_sk').html(filename);
            }
            
        }else {
            alert ("file harus berjenis 'pdf' ");
            $('#file_sk').val("");
            
        }
        
        
    }

}

// upload foto perangkat
$("#foto_perangkat").change(function(event) {
    getURL(this);
});


function getURL(input) {
if (input.files && input.files[0]) {
    var reader = new FileReader();
    var filename = $("#foto_perangkat").val();
    filename = filename.substring(filename.lastIndexOf('\\') + 1);
    var cekgb = filename.substring(filename.lastIndexOf('.') + 1);
    if (cekgb == 'jpg' || cekgb == 'JPG' || cekgb == 'png' || cekgb == 'jpeg' || cekgb == 'jfif') {
        
        $('.foto_p').show();
        $('.nfile').html(filename);

        reader.onload = function(e) {
            debugger;
            $('.gb_perangkat').attr('src', e.target.result);
            $('.gb_perangkat').hide();
            $('.gb_perangkat').fadeIn(500);

                }
    reader.readAsDataURL(input.files[0]);


    }else {
        alert ("file harus berjenis 'jpg' , 'jpeg', 'png', atau 'jfif'");
        $('#foto_perangkat').val("");
        // $('#gb_perangkat').attr('src', '../img/foto_pegawai/no_image.png');

        
    }
    
    // reader.readAsDataURL(input.files[0]);
}


    

}

</script>
@endpush