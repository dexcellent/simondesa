@extends('templates.desa.main')
@section('css')
<style>
    #tabaktif {
        background-color: aqua;
        color: black;
    }
</style>
@endsection
@section('content')
<div class="container">
    <h3>FORM INPUT / UPDATE DATA PERANGKAT DESA</h3>

    <div class="col-md-12 col-sm-12  ">
        <div class="x_panel">
            <div class="x_title">


                <form class="form-inline" action="/adminDesa/formPerangkat" method="get">
                    @csrf
                    <div class="form-group mx-sm-3 mb-2">
                        <h6>Masukkan tahun data :</h6>
                        <input type="text" name="tahun" class="form-control ml-3" placeholder="{{ $tahun }}"
                            data-inputmask="'mask': '9999'">
                    </div>
                    <button type="submit" class="btn btn-primary mb-2">Cek Data</button>
                </form>
                <ul class="nav navbar-right panel_toolbox">
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                    </li>
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button"
                            aria-expanded="false"><i class="fa fa-wrench"></i></a>

                    </li>
                    <li><a class="close-link"><i class="fa fa-close"></i></a>
                    </li>
                </ul>
                <div class="clearfix"></div>
            </div>
            <div>Tahun Data : {{ $tahun }}</div>
            <div class="x_content">

                <ul class="nav nav-tabs bar_tabs" id="myTab" role="tablist">
                    <li class="nav-item">
                        <a class="nav-link" href="?jabatan=Kepala Desa&tahun={{ $tahun }}" role="tab"
                            aria-selected="true">Kades <span
                                class="fa fa-check-circle ml-1 {{ $kades==0 ? 'd-none' : '' }}"></span></a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" id="profile-tab" href="?jabatan=Sekretaris Desa&tahun={{ $tahun }}">
                            Sekdes <span class="fa fa-check-circle ml-1 {{ $sekdes==0 ? 'd-none' : '' }}"></span>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="?jabatan=Kaur Umum&tahun={{ $tahun }}" role="tab"
                            aria-selected="true">
                            Kaur Umum <span class="fa fa-check-circle ml-1 {{ $kaur_umum==0 ? 'd-none' : '' }}"></span>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="?jabatan=Kaur Perencanaan&tahun={{ $tahun }}" role="tab"
                            aria-selected="true">
                            Kaur Perencanaan <span
                                class="fa fa-check-circle ml-1 {{ $kaur_per==0 ? 'd-none' : '' }}"></span>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="?jabatan=Kaur Keuangan&tahun={{ $tahun }}" role="tab"
                            aria-selected="true">
                            Kaur Keuangan <span
                                class="fa fa-check-circle ml-1 {{ $kaur_keu==0 ? 'd-none' : '' }}"></span>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="?jabatan=Kasi Pemerintahan&tahun={{ $tahun }}" role="tab"
                            aria-selected="true">Kasi
                            Pemerintahan <span
                                class="fa fa-check-circle ml-1 {{ $kasi_pem==0 ? 'd-none' : '' }}"></span>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="?jabatan=Kasi Kesra&tahun={{ $tahun }}" role="tab"
                            aria-selected="true">Kasi
                            Kesra <span class="fa fa-check-circle ml-1 {{ $kasi_kesra==0 ? 'd-none' : '' }}"></span>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="?jabatan=Kasi Pelayanan&tahun={{ $tahun }}" role="tab"
                            aria-selected="true">Kasi
                            Pelayanan <span class="fa fa-check-circle ml-1 {{ $kasi_pel==0 ? 'd-none' : '' }}"></span>
                        </a>
                    </li>
                </ul>
                <div class="tab-content" id="myTabContent">
                    <div class="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">
                        <h4 class="text-center">Pilih Tab</h4>
                    </div>

                </div>
                <br><br><br>
            </div>
        </div>
    </div>
    <br>
    <br>

</div>


@endsection
@push('script')
<!-- jquery.inputmask -->
<script src="/vendors/jquery.inputmask/dist/min/jquery.inputmask.bundle.min.js"></script>
<script>
    $("#file_sk").change(function(event) {

        getURL(this);
    });

function getURL(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();
        var filename = $("#file_sk").val();
        
        filename = filename.substring(filename.lastIndexOf('\\') + 1);
        var cekgb = filename.substring(filename.lastIndexOf('.') + 1);
        if (cekgb == 'pdf' || cekgb == 'PDF') {
            if(input.files[0]['size'] > 1024){
                alert('ukuran file tidak boleh > 1 Mb !');
                $('#file_sk').val("");
            }else{
                $('#nmfile_sk').html('');
                $('#nmfile_sk').html(filename);
            }
            
        }else {
            alert ("file harus berjenis 'pdf' ");
            $('#file_sk').val("");
            
        }
        
        
    }

}

// upload foto perangkat
$("#foto_perangkat").change(function(event) {
    getURL(this);
});


function getURL(input) {
if (input.files && input.files[0]) {
    var reader = new FileReader();
    var filename = $("#foto_perangkat").val();
    filename = filename.substring(filename.lastIndexOf('\\') + 1);
    var cekgb = filename.substring(filename.lastIndexOf('.') + 1);
    if (cekgb == 'jpg' || cekgb == 'JPG' || cekgb == 'png' || cekgb == 'jpeg' || cekgb == 'jfif') {
        
        $('.foto_p').show();
        $('.nfile').html(filename);

        reader.onload = function(e) {
            debugger;
            $('.gb_perangkat').attr('src', e.target.result);
            $('.gb_perangkat').hide();
            $('.gb_perangkat').fadeIn(500);

                }
    reader.readAsDataURL(input.files[0]);


    }else {
        alert ("file harus berjenis 'jpg' , 'jpeg', 'png', atau 'jfif'");
        $('#foto_perangkat').val("");
        // $('#gb_perangkat').attr('src', '../img/foto_pegawai/no_image.png');

        
    }
    
    // reader.readAsDataURL(input.files[0]);
}


    

}

</script>
@endpush