{{-- Form Edit Datum Kewilayahan --}}
@extends('templates.desa.main')

@section('content')
<div class="row justify-content-center mb-4">
    <div class=" col-md-8">
        <table class="table table-striped " style="font-size: .9rem">
            <thead>
                <tr>
                    <th colspan="2" class="text-center bg-blue">FORM UPDATE DATA KEWILAYAHAN</th>
                </tr>
            </thead>
            <tbody>
                <form action="/adminDesa/updateDatumWil" method="post">
                    @csrf
                    <tr>
                        <td>
                            <label for="dasar" class="col-form-label">Dasar Hukum Pembentukan Desa</label>
                        </td>
                        <td>
                            <textarea class="form-control" name="dasar_hukum" id="dasar" rows="2"
                                style="font-size: .9rem" autofocus>{{ $wilayahs['dasar_hukum'] }}</textarea>
                        </td>

                    </tr>
                    <tr>
                        <td><label for="wilayah" class="col-form-label">Luas Wilayah</label></td>
                        <td class="d-flex"><input type="text" class="form-control" name="luas" required
                                value="{{ $wilayahs['luas'] }}"> <span class="input-group-text" id="wilayah"
                                style="font-size: .9rem">Km2</span>
                            <input type="hidden" name="asal_id" value="{{ $wilayahs['asal_id'] }}">

                        </td>

                    </tr>
                    <tr>
                        <td><label for="utara" class="col-form-label">Sebelah Utara berbatasan dengan</label></td>
                        <td><textarea name="batas_utara" class="form-control" id="utara" rows="2"
                                style="font-size: .9rem" required>{{ $wilayahs['batas_utara'] }}</textarea></td>

                    </tr>
                    <tr>
                        <td><label for="selatan" class="col-form-label">Sebelah Selatan berbatasan dengan</label></td>
                        <td><textarea name="batas_selatan" class="form-control" id="selatan" rows="2"
                                style="font-size: .9rem" required>{{ $wilayahs['batas_selatan'] }}</textarea></td>

                    </tr>
                    <tr>
                        <td><label for="barat" class="col-form-label">Sebelah Barat berbatasan dengan</label></td>
                        <td><textarea name="batas_barat" class="form-control" id="barat" rows="2"
                                style="font-size: .9rem" required>{{ $wilayahs['batas_barat'] }}</textarea></td>

                    </tr>
                    <tr>
                        <td><label for="timur" class="col-form-label">Sebelah Timur berbatasan dengan</label></td>
                        <td><textarea name="batas_timur" class="form-control" id="timur" rows="2"
                                style="font-size: .9rem" required>{{ $wilayahs['batas_timur'] }}</textarea></td>

                    </tr>
                    <tr>
                        <td><label for="dusun" class="col-form-label">Jumlah Dusun</label></td>
                        <td class="d-flex"><input type="number" name="jumlah_dusun" id="dusun" class="form-control"
                                style="font-size: .9rem" required value="{{ $wilayahs['jumlah_dusun'] }}">
                            <span class="input-group-text" style="font-size: .9rem">Dusun</span>
                        </td>

                    </tr>
                    <tr>
                        <td>Jumlah RT</td>
                        <td class="d-flex">
                            <input type="number" name="jumlah_rt" class="form-control" style="font-size: .9rem" required
                                value="{{ $wilayahs['jumlah_rt'] }}"> <span class="input-group-text"
                                style="font-size: .9rem">Rukun
                                Tetangga</span>
                        </td>

                    </tr>
                    <tr>
                        <td>Jumlah Pimpinan dan Anggota BPD</td>
                        <td class="d-flex">
                            <input type="number" name="jumlah_bpd" class="form-control" style="font-size: .9rem"
                                required value="{{ $wilayahs['jumlah_bpd'] }}"> <span class="input-group-text"
                                style="font-size: .9rem">Orang</span>
                        </td>

                    </tr>
                    <tr>
                        <td colspan="2" align="center">
                            <button class="btn btn-primary btn-sm">UPDATE DATA</button>
                        </td>
                    </tr>
                </form>
            </tbody>
        </table>
    </div>
</div>

{{-- notifikasi --}}
@if(session()->has('updated'))
<script>
    Swal.fire({
  position: 'top-end',
  icon: 'success',
  title: '{{ session("updated") }}',
  showConfirmButton: false,
  timer: 1500
})
</script>

@endif
@if(session()->has('tambah'))
<script>
    Swal.fire({
  position: 'top-end',
  icon: 'success',
  title: '{{ session("tambah") }}',
  showConfirmButton: false,
  timer: 1500
})
</script>

@endif
@endsection