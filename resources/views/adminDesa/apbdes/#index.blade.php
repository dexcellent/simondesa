@extends('templates.desa.main')

@section('content')

    <div class="">
        <div class="page-title">
            <div class="col-md-12">
                <div class="row">
                    <div class="col-md-6">
                        <h4>APBDes</h4>
                        <button type="button" class="btn btn-sm text-white" style="background-color: #38516a;" data-toggle="modal" data-target="#pilihSemester"><i class="fa fa-calendar"></i> Tahun  - Semester  </button>
                    </div>
                    <div class="col-md-6">
                        <h5><span class="badge text-white" style="background-color: #3ca0ac;">Desa : </span></h5>
                        <h5><span class="badge text-white" style="background-color: #3ca0ac;">Kecamatan : </span></h5>
                    </div>
                </div>
            </div>
        </div>

        <div id="pilihSemester" class="modal fade" role="dialog" aria-hidden="true">
            <div class="modal-dialog modal-sm">
                <div class="modal-content">
                    <div class="modal-header text-white" style="background-color: #38516a;">
                        <h6 class="modal-title" id="myModalLabel">Pilih Tahun & Semester</h6>
                        <button type="button" style="color: white;" class="close" data-dismiss="modal"><span aria-hidden="true">×</span>
                        </button>
                    </div>
                    <form action="#" method="post" enctype="multipart/form-data" class="form-horizontal mb-lg">
                        <div class="modal-body">
                            @csrf
                            <div class="form-group row">
                                <label for="username">Tahun</label>
                                <select class="form-control select2bs4" style="width: 100%;" name="tahun" required oninvalid="this.setCustomValidity('Mohon pilih Tahun')" oninput="setCustomValidity('')">
                                    <option value="" selected disabled>--Pilih Tahun--</option>
                                    <option value="2022">2022</option>
                                    <option value="2023">2023</option>
                                </select>
                            </div>
                            <div class="form-group row">
                                <label for="username">Semester</label>
                                <select class="form-control select2bs4" style="width: 100%;" name="semester" required oninvalid="this.setCustomValidity('Mohon pilih Semester')" oninput="setCustomValidity('')">
                                    <option value="" selected disabled>--Pilih Semester--</option>
                                    <option value="1">Semester 1</option>
                                    <option value="2">Semester 2</option>
                                </select>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-sm" style="background-color: #e5eefe;" data-dismiss="modal">Batal</button>
                            <button type="submit" class="btn btn-sm text-white" style="background-color: #49bbc6;"><i class="fa fa-check"></i> Pilih</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>

        <div class="clearfix"></div>

        <div class="row">

            <div class="col-md-12">

                <div class="x_panel">
                    <div class="x_content">

                        <ul class="nav nav-tabs bar_tabs" id="apbdesTab" role="tablist">
                            <li class="nav-item">
                                <a class="nav-link active" data-toggle="tab" href="#murni" role="tab" aria-controls="murni" aria-selected="true">Murni</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" data-toggle="tab" href="#perubahan" role="tab" aria-controls="perubahan" aria-selected="false">Perubahan</a>
                            </li>
                        </ul>

                        <div class="tab-content" id="apbdesTabContent">

                            <div class="tab-pane fade show active" id="murni" role="tabpanel" aria-labelledby="murni-tab">

                                <div class="col-md-12">
                                    <div class="x_panel">
                                        <div class="x_title">
                                            <h2><small>APBDes Murni &emsp; Tahun : </small></h2>
                                            
                                                <a type="button" href="#" class="btn text-white btn-sm pull-right" style="background-color: #49bbc6;"><i class="fa fa-plus"></i> Tambah Data</a>
                                            

                                            <div class="clearfix"></div>

                                            <div class="ln_solid"></div>
                                            <div class="progress progress_sm">
                                                <div class="progress-bar bg-green" role="progressbar" data-transitiongoal=""></div>
                                            </div>
                                            <h5>% Data telah terisi</h5>
                                        </div>
                                        <?php foreach ($apbdes as $mur) : ?>
                                            <div class="x_content">
                                                <form action="#" method="post" enctype="multipart/form-data" class="form-horizontal mb-lg">
                                                    @csrf
                                                    <div class="ln_solid" style="border-width: 5px;"></div>
                                                    <h6 style="text-align: center; font-weight:bold;">Pendapatan Desa <a type="button" onclick="ubahpendapatanmurni()" class="btn text-white btn-sm pull-right" style="background-color: #49bbc6;"><i class="fa fa-edit"></i> Ubah</a></h6>
                                                    <div class="ln_solid" style="border-width: 5px;"></div>

                                                    <div class="form-group row">
                                                        <label class="control-label col-md-3 col-sm-3">Pendapatan Desa Seluruhnya</label>
                                                        <div class="col-md-9 col-sm-9">
                                                            <input type="text" class="form-control" style="text-align: right;" value="" maxlength="30" readonly />
                                                            <span class="form-control-feedback left" aria-hidden="true"> Rp</span>
                                                        </div>
                                                    </div>
                                                    <div class="form-group row">
                                                        <label class="control-label col-md-2 col-sm-2 offset-md-3" style="font-style: italic;">Pendapatan Asli Desa</label>
                                                        <div class="col-md-7 col-sm-7">
                                                            <input type="text" class="form-control" style="text-align: right;" id="pad" name="pad" value="" maxlength="30" disabled required oninvalid="this.setCustomValidity('Mohon isi Jumlah Dusun')" oninput="setCustomValidity('')" />
                                                            <span class="form-control-feedback left" aria-hidden="true"> Rp</span>
                                                        </div>
                                                    </div>
                                                    <div class="form-group row">
                                                        <label class="control-label col-md-2 col-sm-2 offset-md-3" style="font-style: italic;">Pendapatan Transfer</label>
                                                        <div class="col-md-7 col-sm-7">
                                                            <input type="text" class="form-control right" style="text-align: right;" id="pt" name="pt" value="" maxlength="30" readonly />
                                                            <span class="form-control-feedback left" aria-hidden="true"> Rp</span>
                                                        </div>
                                                    </div>
                                                    <div class="form-group row">
                                                        <label class="control-label col-md-2 col-sm-2 offset-md-5" style="font-style: italic;">Dana Desa</label>
                                                        <div class="col-md-5 col-sm-5">
                                                            <input type="text" class="form-control right" style="text-align: right;" id="dd" name="dd" value="" maxlength="30" disabled required oninvalid="this.setCustomValidity('Mohon isi Dana Desa')" oninput="setCustomValidity('')" />
                                                            <span class="form-control-feedback left" aria-hidden="true"> Rp</span>
                                                        </div>
                                                    </div>
                                                    <div class="form-group row">
                                                        <label class="control-label col-md-2 col-sm-2 offset-md-5" style="font-style: italic;">Alokasi Dana Desa</label>
                                                        <div class="col-md-5 col-sm-5">
                                                            <input type="text" class="form-control uang" style="text-align: right;" id="alokasi_dd" name="alokasi_dd" value="" maxlength="30" disabled required oninvalid="this.setCustomValidity('Mohon isi Alokasi Dana Desa')" oninput="setCustomValidity('')" />
                                                            <span class="form-control-feedback left" aria-hidden="true"> Rp</span>
                                                        </div>
                                                    </div>
                                                    <div class="form-group row">
                                                        <label class="control-label col-md-2 col-sm-2 offset-md-5" style="font-style: italic;">Dana Bagi Hasil</label>
                                                        <div class="col-md-5 col-sm-5">
                                                            <input type="text" class="form-control uang" style="text-align: right;" id="dbh" name="dbh" value="" maxlength="30" disabled required oninvalid="this.setCustomValidity('Mohon isi Dana Bagi Hasil')" oninput="setCustomValidity('')" />
                                                            <span class="form-control-feedback left" aria-hidden="true"> Rp</span>
                                                        </div>
                                                    </div>
                                                    <div class="form-group row">
                                                        <label class="control-label col-md-2 col-sm-2 offset-md-3" style="font-style: italic;">Pendapatan Lainnya</label>
                                                        <div class="col-md-7 col-sm-7">
                                                            <input type="text" class="form-control uang" style="text-align: right;" id="pl" name="pl" value="" maxlength="30" disabled required oninvalid="this.setCustomValidity('Mohon isi Pendapatan Lainnya')" oninput="setCustomValidity('')" />
                                                            <span class="form-control-feedback left" aria-hidden="true"> Rp</span>
                                                        </div>
                                                    </div>

                                                    <div class="ln_solid"></div>
                                                    <div class="form-group">
                                                        <div class="col-md-12 col-sm-12" style="text-align: center;">
                                                            <button type="reset" class="btn btn-sm" style="background-color: #e5eefe;" id="batal_pendapatanmurni" onclick="batalpendapatanmurni()" disabled>Batal</button>
                                                            <button type="submit" id="submit_pendapatanmurni" class="btn text-white btn-sm" style="background-color: #49bbc6;" disabled><i class="fa fa-save"></i> Simpan</button>
                                                        </div>
                                                    </div>
                                                </form>


                                                <div class="ln_solid" style="border-width: 5px;"></div>
                                                <h6 style="text-align: center; font-weight:bold;">Belanja Desa <a type="button" onclick="ubahbelanjamurni()" class="btn text-white btn-sm pull-right" style="background-color: #49bbc6;"><i class="fa fa-edit"></i> Ubah</a></h6>
                                                <div class="ln_solid" style="border-width: 5px;"></div>

                                                <form action="#" method="post" enctype="multipart/form-data" class="form-horizontal mb-lg">
                                                    <div class="row">
                                                        @csrf
                                                        <div class="col-md-3">
                                                            <label class="control-label col-md-12">Bidang Pemerintahan Desa</label>
                                                        </div>
                                                        <div class="col-md-9">
                                                            <div class="form-group row">
                                                                <label class="control-label col-md-3" style="font-style: italic;">Belanja Pegawai</label>
                                                                <div class="col-md-9">
                                                                    <input type="text" class="form-control" style="text-align: right;" id="belanja_pegawai_pemdes" name="belanja_pegawai_pemdes" value="" maxlength="30" disabled />
                                                                    <span class="form-control-feedback left" aria-hidden="true"> Rp</span>
                                                                </div>
                                                            </div>
                                                            <div class="form-group row">
                                                                <label class="control-label col-md-3" style="font-style: italic;">Belanja Modal</label>
                                                                <div class="col-md-9">
                                                                    <input type="text" class="form-control" style="text-align: right;" id="belanja_modal_pemdes" name="belanja_modal_pemdes" value="" maxlength="30" disabled />
                                                                    <span class="form-control-feedback left" aria-hidden="true"> Rp</span>
                                                                </div>
                                                            </div>
                                                            <div class="form-group row">
                                                                <label class="control-label col-md-3" style="font-style: italic;">Belanja Barang dan Jasa</label>
                                                                <div class="col-md-9">
                                                                    <input type="text" class="form-control" style="text-align: right;" id="belanja_barjas_pemdes" name="belanja_barjas_pemdes" value="" maxlength="30" disabled />
                                                                    <span class="form-control-feedback left" aria-hidden="true"> Rp</span>
                                                                </div>
                                                            </div>
                                                            <div class="form-group row">
                                                                <label class="control-label col-md-3" style="font-style: italic;">Belanja Tak Terduga</label>
                                                                <div class="col-md-9">
                                                                    <input type="text" class="form-control" style="text-align: right;" id="belanja_takterduga_pemdes" name="belanja_takterduga_pemdes" value="" maxlength="30" disabled />
                                                                    <span class="form-control-feedback left" aria-hidden="true"> Rp</span>
                                                                </div>
                                                            </div>
                                                            <div class="form-group row">
                                                                <label class="control-label col-md-3" style="font-style: italic;">Total Belanja</label>
                                                                <div class="col-md-9">
                                                                    <input type="text" class="form-control" style="text-align: right;" id="belanja_total_pemdes" name="belanja_total_pemdes" value="" maxlength="30" readonly />
                                                                    <span class="form-control-feedback left" aria-hidden="true"> Rp</span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="ln_solid" style="border-width: 5px;"></div>
                                                    <div class="row">
                                                        <div class="col-md-3">
                                                            <label class="control-label col-md-12">Bidang Pelaksanaan Pembangunan Desa</label>
                                                        </div>
                                                        <div class="col-md-9">
                                                            <div class="form-group row">
                                                                <label class="control-label col-md-3" style="font-style: italic;">Belanja Pegawai</label>
                                                                <div class="col-md-9">
                                                                    <input type="text" class="form-control" style="text-align: right;" id="belanja_pegawai_pembangunan" name="belanja_pegawai_pembangunan" value="" maxlength="30" disabled />
                                                                    <span class="form-control-feedback left" aria-hidden="true"> Rp</span>
                                                                </div>
                                                            </div>
                                                            <div class="form-group row">
                                                                <label class="control-label col-md-3" style="font-style: italic;">Belanja Modal</label>
                                                                <div class="col-md-9">
                                                                    <input type="text" class="form-control" style="text-align: right;" id="belanja_modal_pembangunan" name="belanja_modal_pembangunan" value="" maxlength="30" disabled />
                                                                    <span class="form-control-feedback left" aria-hidden="true"> Rp</span>
                                                                </div>
                                                            </div>
                                                            <div class="form-group row">
                                                                <label class="control-label col-md-3" style="font-style: italic;">Belanja Barang dan Jasa</label>
                                                                <div class="col-md-9">
                                                                    <input type="text" class="form-control" style="text-align: right;" id="belanja_barjas_pembangunan" name="belanja_barjas_pembangunan" value="" maxlength="30" disabled />
                                                                    <span class="form-control-feedback left" aria-hidden="true"> Rp</span>
                                                                </div>
                                                            </div>
                                                            <div class="form-group row">
                                                                <label class="control-label col-md-3" style="font-style: italic;">Belanja Tak Terduga</label>
                                                                <div class="col-md-9">
                                                                    <input type="text" class="form-control" style="text-align: right;" id="belanja_takterduga_pembangunan" name="belanja_takterduga_pembangunan" value="" maxlength="30" disabled />
                                                                    <span class="form-control-feedback left" aria-hidden="true"> Rp</span>
                                                                </div>
                                                            </div>
                                                            <div class="form-group row">
                                                                <label class="control-label col-md-3" style="font-style: italic;">Total Belanja</label>
                                                                <div class="col-md-9">
                                                                    <input type="text" class="form-control" style="text-align: right;" id="belanja_total_pembangunan" name="belanja_total_pembangunan" value="" maxlength="30" readonly />
                                                                    <span class="form-control-feedback left" aria-hidden="true"> Rp</span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="ln_solid" style="border-width: 5px;"></div>
                                                    <div class="row">
                                                        <div class="col-md-3">
                                                            <label class="control-label col-md-12">Bidang Pembinaan Kemasyarakatan</label>
                                                        </div>
                                                        <div class="col-md-9">
                                                            <div class="form-group row">
                                                                <label class="control-label col-md-3" style="font-style: italic;">Belanja Pegawai</label>
                                                                <div class="col-md-9">
                                                                    <input type="text" class="form-control" style="text-align: right;" id="belanja_pegawai_binamasyarakat" name="belanja_pegawai_binamasyarakat" value="" maxlength="30" disabled />
                                                                    <span class="form-control-feedback left" aria-hidden="true"> Rp</span>
                                                                </div>
                                                            </div>
                                                            <div class="form-group row">
                                                                <label class="control-label col-md-3" style="font-style: italic;">Belanja Modal</label>
                                                                <div class="col-md-9">
                                                                    <input type="text" class="form-control" style="text-align: right;" id="belanja_modal_binamasyarakat" name="belanja_modal_binamasyarakat" value="" maxlength="30" disabled />
                                                                    <span class="form-control-feedback left" aria-hidden="true"> Rp</span>
                                                                </div>
                                                            </div>
                                                            <div class="form-group row">
                                                                <label class="control-label col-md-3" style="font-style: italic;">Belanja Barang dan Jasa</label>
                                                                <div class="col-md-9">
                                                                    <input type="text" class="form-control" style="text-align: right;" id="belanja_barjas_binamasyarakat" name="belanja_barjas_binamasyarakat" value="" maxlength="30" disabled />
                                                                    <span class="form-control-feedback left" aria-hidden="true"> Rp</span>
                                                                </div>
                                                            </div>
                                                            <div class="form-group row">
                                                                <label class="control-label col-md-3" style="font-style: italic;">Belanja Tak Terduga</label>
                                                                <div class="col-md-9">
                                                                    <input type="text" class="form-control" style="text-align: right;" id="belanja_takterduga_binamasyarakat" name="belanja_takterduga_binamasyarakat" value="" maxlength="30" disabled />
                                                                    <span class="form-control-feedback left" aria-hidden="true"> Rp</span>
                                                                </div>
                                                            </div>
                                                            <div class="form-group row">
                                                                <label class="control-label col-md-3" style="font-style: italic;">Total Belanja</label>
                                                                <div class="col-md-9">
                                                                    <input type="text" class="form-control" style="text-align: right;" id="belanja_total_binamasyarakat" name="belanja_total_binamasyarakat" value="" maxlength="30" readonly />
                                                                    <span class="form-control-feedback left" aria-hidden="true"> Rp</span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="ln_solid" style="border-width: 5px;"></div>
                                                    <div class="row">
                                                        <div class="col-md-3">
                                                            <label class="control-label col-md-12">Bidang Pemberdayaan Masyarakat</label>
                                                        </div>
                                                        <div class="col-md-9">
                                                            <div class="form-group row">
                                                                <label class="control-label col-md-3" style="font-style: italic;">Belanja Pegawai</label>
                                                                <div class="col-md-9">
                                                                    <input type="text" class="form-control" style="text-align: right;" id="belanja_pegawai_berdayamasyarakat" name="belanja_pegawai_berdayamasyarakat" value="" maxlength="30" disabled />
                                                                    <span class="form-control-feedback left" aria-hidden="true"> Rp</span>
                                                                </div>
                                                            </div>
                                                            <div class="form-group row">
                                                                <label class="control-label col-md-3" style="font-style: italic;">Belanja Modal</label>
                                                                <div class="col-md-9">
                                                                    <input type="text" class="form-control" style="text-align: right;" id="belanja_modal_berdayamasyarakat" name="belanja_modal_berdayamasyarakat" value="" maxlength="30" disabled />
                                                                    <span class="form-control-feedback left" aria-hidden="true"> Rp</span>
                                                                </div>
                                                            </div>
                                                            <div class="form-group row">
                                                                <label class="control-label col-md-3" style="font-style: italic;">Belanja Barang dan Jasa</label>
                                                                <div class="col-md-9">
                                                                    <input type="text" class="form-control" style="text-align: right;" id="belanja_barjas_berdayamasyarakat" name="belanja_barjas_berdayamasyarakat" value="" maxlength="30" disabled />
                                                                    <span class="form-control-feedback left" aria-hidden="true"> Rp</span>
                                                                </div>
                                                            </div>
                                                            <div class="form-group row">
                                                                <label class="control-label col-md-3" style="font-style: italic;">Belanja Tak Terduga</label>
                                                                <div class="col-md-9">
                                                                    <input type="text" class="form-control" style="text-align: right;" id="belanja_takterduga_berdayamasyarakat" name="belanja_takterduga_berdayamasyarakat" value="" maxlength="30" disabled />
                                                                    <span class="form-control-feedback left" aria-hidden="true"> Rp</span>
                                                                </div>
                                                            </div>
                                                            <div class="form-group row">
                                                                <label class="control-label col-md-3" style="font-style: italic;">Total Belanja</label>
                                                                <div class="col-md-9">
                                                                    <input type="text" class="form-control" style="text-align: right;" id="belanja_total_berdayamasyarakat" name="belanja_total_berdayamasyarakat" value="" maxlength="30" readonly />
                                                                    <span class="form-control-feedback left" aria-hidden="true"> Rp</span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="ln_solid" style="border-width: 5px;"></div>
                                                    <div class="row">
                                                        <div class="col-md-3">
                                                            <label class="control-label col-md-12">Bidang Penanggulangan Bencana Darurat</label>
                                                        </div>
                                                        <div class="col-md-9">
                                                            <div class="form-group row">
                                                                <label class="control-label col-md-3" style="font-style: italic;">Belanja Pegawai</label>
                                                                <div class="col-md-9">
                                                                    <input type="text" class="form-control" style="text-align: right;" id="belanja_pegawai_pbd" name="belanja_pegawai_pbd" value="" maxlength="30" disabled />
                                                                    <span class="form-control-feedback left" aria-hidden="true"> Rp</span>
                                                                </div>
                                                            </div>
                                                            <div class="form-group row">
                                                                <label class="control-label col-md-3" style="font-style: italic;">Belanja Modal</label>
                                                                <div class="col-md-9">
                                                                    <input type="text" class="form-control" style="text-align: right;" id="belanja_modal_pbd" name="belanja_modal_pbd" value="" maxlength="30" disabled />
                                                                    <span class="form-control-feedback left" aria-hidden="true"> Rp</span>
                                                                </div>
                                                            </div>
                                                            <div class="form-group row">
                                                                <label class="control-label col-md-3" style="font-style: italic;">Belanja Barang dan Jasa</label>
                                                                <div class="col-md-9">
                                                                    <input type="text" class="form-control" style="text-align: right;" id="belanja_barjas_pbd" name="belanja_barjas_pbd" value="" maxlength="30" disabled />
                                                                    <span class="form-control-feedback left" aria-hidden="true"> Rp</span>
                                                                </div>
                                                            </div>
                                                            <div class="form-group row">
                                                                <label class="control-label col-md-3" style="font-style: italic;">Belanja Tak Terduga</label>
                                                                <div class="col-md-9">
                                                                    <input type="text" class="form-control" style="text-align: right;" id="belanja_takterduga_pbd" name="belanja_takterduga_pbd" value="" maxlength="30" disabled />
                                                                    <span class="form-control-feedback left" aria-hidden="true"> Rp</span>
                                                                </div>
                                                            </div>
                                                            <div class="form-group row">
                                                                <label class="control-label col-md-3" style="font-style: italic;">Total Belanja</label>
                                                                <div class="col-md-9">
                                                                    <input type="text" class="form-control" style="text-align: right;" id="belanja_total_pbd" name="belanja_total_pbd" value="" maxlength="30" readonly />
                                                                    <span class="form-control-feedback left" aria-hidden="true"> Rp</span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="ln_solid"></div>
                                                    <div class="form-group">
                                                        <div class="col-md-12 col-sm-12" style="text-align: center;">
                                                            <button type="reset" class="btn btn-sm" style="background-color: #e5eefe;" id="batal_belanjamurni" onclick="batalbelanjamurni()" disabled>Batal</button>
                                                            <button type="submit" id="submit_belanjamurni" class="btn text-white btn-sm" style="background-color: #49bbc6;" disabled><i class="fa fa-save"></i> Simpan</button>
                                                        </div>
                                                    </div>

                                                </form>

                                                <div class="ln_solid" style="border-width: 5px;"></div>
                                                <h6 style="text-align: center; font-weight:bold;">Pembiayaan Desa <a type="button" onclick="ubahpembiayaanmurni()" class="btn text-white btn-sm pull-right" style="background-color: #49bbc6;"><i class="fa fa-edit"></i> Ubah</a></h6>
                                                <div class="ln_solid" style="border-width: 5px;"></div>

                                                <form action="#" method="post" enctype="multipart/form-data" class="form-horizontal mb-lg">
                                                    @csrf
                                                    <div class="form-group row">
                                                        <label class="control-label col-md-3" style="font-style: italic;">Penerimaan Pembiayaan</label>
                                                        <div class="col-md-9">
                                                            <input type="text" class="form-control" style="text-align: right;" id="penerimaan_biaya" name="penerimaan_biaya" value="" maxlength="30" disabled />
                                                            <span class="form-control-feedback left" aria-hidden="true"> Rp</span>
                                                        </div>
                                                    </div>
                                                    <div class="form-group row">
                                                        <label class="control-label col-md-3" style="font-style: italic;">Pengeluaran Pembiayaan</label>
                                                        <div class="col-md-9">
                                                            <input type="text" class="form-control" style="text-align: right;" id="pengeluaran_biaya" name="pengeluaran_biaya" value="" maxlength="30" disabled />
                                                            <span class="form-control-feedback left" aria-hidden="true"> Rp</span>
                                                        </div>
                                                    </div>
                                                    <div class="ln_solid"></div>
                                                    <div class="form-group">
                                                        <div class="col-md-12 col-sm-12" style="text-align: center;">
                                                            <button type="reset" class="btn btn-sm" style="background-color: #e5eefe;" id="batal_pembiayaanmurni" onclick="batalpembiayaanmurni()" disabled>Batal</button>
                                                            <button type="submit" id="submit_pembiayaanmurni" class="btn text-white btn-sm" style="background-color: #49bbc6;" disabled><i class="fa fa-save"></i> Simpan</button>
                                                        </div>
                                                    </div>
                                                </form>

                                            </div>
                                        <?php endforeach; ?>
                                    </div>
                                </div>
                            </div>

                            <div class="tab-pane fade" id="perubahan" role="tabpanel" aria-labelledby="perubahan-tab">

                                <div class="col-md-12 ">
                                    <div class="x_panel">
                                        <div class="x_title">
                                            <h2><small>APBDes Perubahan Tahun </small></h2>

                                            
                                                <a type="button" href="#" class="btn text-white btn-sm pull-right" style="background-color: #49bbc6;"><i class="fa fa-plus"></i> Tambah Data</a>
                                            

                                            <div class="clearfix"></div>

                                            <div class="ln_solid"></div>
                                            <div class="progress progress_sm">
                                                <div class="progress-bar bg-green" role="progressbar" data-transitiongoal=""></div>
                                            </div>
                                            <h5>% Data telah terisi</h5>

                                        </div>

                                        <?php foreach ($apbdes as $per) : ?>
                                            <div class="x_content">
                                                <form action="#" method="post" enctype="multipart/form-data" class="form-horizontal mb-lg">
                                                    @csrf
                                                    <div class="ln_solid" style="border-width: 5px;"></div>
                                                    <h6 style="text-align: center; font-weight:bold;">Pendapatan Desa <a type="button" onclick="ubahpendapatanperubahan()" class="btn text-white btn-sm pull-right" style="background-color: #49bbc6;"><i class="fa fa-edit"></i> Ubah</a></h6>
                                                    <div class="ln_solid" style="border-width: 5px;"></div>

                                                    <div class="form-group row">
                                                        <label class="control-label col-md-3 col-sm-3">Pendapatan Desa Seluruhnya</label>
                                                        <div class="col-md-9 col-sm-9">
                                                            <input type="text" class="form-control" style="text-align: right;" value="" maxlength="30" readonly />
                                                            <span class="form-control-feedback left" aria-hidden="true"> Rp</span>
                                                        </div>
                                                    </div>
                                                    <div class="form-group row">
                                                        <label class="control-label col-md-2 col-sm-2 offset-md-3" style="font-style: italic;">Pendapatan Asli Desa</label>
                                                        <div class="col-md-7 col-sm-7">
                                                            <input type="text" class="form-control" style="text-align: right;" id="padper" name="pad" value="" maxlength="30" disabled required oninvalid="this.setCustomValidity('Mohon isi Jumlah Dusun')" oninput="setCustomValidity('')" />
                                                            <span class="form-control-feedback left" aria-hidden="true"> Rp</span>
                                                        </div>
                                                    </div>
                                                    <div class="form-group row">
                                                        <label class="control-label col-md-2 col-sm-2 offset-md-3" style="font-style: italic;">Pendapatan Transfer</label>
                                                        <div class="col-md-7 col-sm-7">
                                                            <input type="text" class="form-control right" style="text-align: right;" id="ptper" name="pt" value="" maxlength="30" disabled required oninvalid="this.setCustomValidity('Mohon isi Pendapatan Transfer')" oninput="setCustomValidity('')" />
                                                            <span class="form-control-feedback left" aria-hidden="true"> Rp</span>
                                                        </div>
                                                    </div>
                                                    <div class="form-group row">
                                                        <label class="control-label col-md-2 col-sm-2 offset-md-5" style="font-style: italic;">Dana Desa</label>
                                                        <div class="col-md-5 col-sm-5">
                                                            <input type="text" class="form-control right" style="text-align: right;" id="ddper" name="dd" value="" maxlength="30" disabled required oninvalid="this.setCustomValidity('Mohon isi Dana Desa')" oninput="setCustomValidity('')" />
                                                            <span class="form-control-feedback left" aria-hidden="true"> Rp</span>
                                                        </div>
                                                    </div>
                                                    <div class="form-group row">
                                                        <label class="control-label col-md-2 col-sm-2 offset-md-5" style="font-style: italic;">Alokasi Dana Desa</label>
                                                        <div class="col-md-5 col-sm-5">
                                                            <input type="text" class="form-control" style="text-align: right;" id="alokasi_ddper" name="alokasi_dd" value="" maxlength="30" disabled required oninvalid="this.setCustomValidity('Mohon isi Alokasi Dana Desa')" oninput="setCustomValidity('')" />
                                                            <span class="form-control-feedback left" aria-hidden="true"> Rp</span>
                                                        </div>
                                                    </div>
                                                    <div class="form-group row">
                                                        <label class="control-label col-md-2 col-sm-2 offset-md-5" style="font-style: italic;">Dana Bagi Hasil</label>
                                                        <div class="col-md-5 col-sm-5">
                                                            <input type="text" class="form-control" style="text-align: right;" id="dbhper" name="dbh" value="" maxlength="30" disabled required oninvalid="this.setCustomValidity('Mohon isi Dana Bagi Hasil')" oninput="setCustomValidity('')" />
                                                            <span class="form-control-feedback left" aria-hidden="true"> Rp</span>
                                                        </div>
                                                    </div>
                                                    <div class="form-group row">
                                                        <label class="control-label col-md-2 col-sm-2 offset-md-3" style="font-style: italic;">Pendapatan Lainnya</label>
                                                        <div class="col-md-7 col-sm-7">
                                                            <input type="text" class="form-control" style="text-align: right;" id="plper" name="pl" value="" maxlength="30" disabled required oninvalid="this.setCustomValidity('Mohon isi Pendapatan Lainnya')" oninput="setCustomValidity('')" />
                                                            <span class="form-control-feedback left" aria-hidden="true"> Rp</span>
                                                        </div>
                                                    </div>

                                                    <div class="ln_solid"></div>
                                                    <div class="form-group">
                                                        <div class="col-md-12 col-sm-12" style="text-align: center;">
                                                            <button type="reset" class="btn btn-sm" style="background-color: #e5eefe;" id="batal_pendapatanperubahan" onclick="batalpendapatanperubahan()" disabled>Batal</button>
                                                            <button type="submit" id="submit_pendapatanperubahan" class="btn text-white btn-sm" style="background-color: #49bbc6;" disabled><i class="fa fa-save"></i> Simpan</button>
                                                        </div>
                                                    </div>
                                                </form>

                                                <div class="ln_solid" style="border-width: 5px;"></div>
                                                <h6 style="text-align: center; font-weight:bold;">Belanja Desa <a type="button" onclick="ubahbelanjaperubahan()" class="btn text-white btn-sm pull-right" style="background-color: #49bbc6;"><i class="fa fa-edit"></i> Ubah</a></h6>
                                                <div class="ln_solid" style="border-width: 5px;"></div>

                                                <form action="#" method="post" enctype="multipart/form-data" class="form-horizontal mb-lg">
                                                    <div class="row">
                                                        @csrf
                                                        <div class="col-md-3">
                                                            <label class="control-label col-md-12">Bidang Pemerintahan Desa</label>
                                                        </div>
                                                        <div class="col-md-9">
                                                            <div class="form-group row">
                                                                <label class="control-label col-md-3" style="font-style: italic;">Belanja Pegawai</label>
                                                                <div class="col-md-9">
                                                                    <input type="text" class="form-control" style="text-align: right;" id="belanja_pegawai_pemdesper" name="belanja_pegawai_pemdes" value="" maxlength="30" disabled />
                                                                    <span class="form-control-feedback left" aria-hidden="true"> Rp</span>
                                                                </div>
                                                            </div>
                                                            <div class="form-group row">
                                                                <label class="control-label col-md-3" style="font-style: italic;">Belanja Modal</label>
                                                                <div class="col-md-9">
                                                                    <input type="text" class="form-control" style="text-align: right;" id="belanja_modal_pemdesper" name="belanja_modal_pemdes" value="" maxlength="30" disabled />
                                                                    <span class="form-control-feedback left" aria-hidden="true"> Rp</span>
                                                                </div>
                                                            </div>
                                                            <div class="form-group row">
                                                                <label class="control-label col-md-3" style="font-style: italic;">Belanja Barang dan Jasa</label>
                                                                <div class="col-md-9">
                                                                    <input type="text" class="form-control" style="text-align: right;" id="belanja_barjas_pemdesper" name="belanja_barjas_pemdes" value="" maxlength="30" disabled />
                                                                    <span class="form-control-feedback left" aria-hidden="true"> Rp</span>
                                                                </div>
                                                            </div>
                                                            <div class="form-group row">
                                                                <label class="control-label col-md-3" style="font-style: italic;">Belanja Tak Terduga</label>
                                                                <div class="col-md-9">
                                                                    <input type="text" class="form-control" style="text-align: right;" id="belanja_takterduga_pemdesper" name="belanja_takterduga_pemdes" value="" maxlength="30" disabled />
                                                                    <span class="form-control-feedback left" aria-hidden="true"> Rp</span>
                                                                </div>
                                                            </div>
                                                            <div class="form-group row">
                                                                <label class="control-label col-md-3" style="font-style: italic;">Total Belanja</label>
                                                                <div class="col-md-9">
                                                                    <input type="text" class="form-control" style="text-align: right;" id="belanja_total_pemdesper" name="belanja_total_pemdes" value="" maxlength="30" readonly />
                                                                    <span class="form-control-feedback left" aria-hidden="true"> Rp</span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="ln_solid" style="border-width: 5px;"></div>
                                                    <div class="row">
                                                        <div class="col-md-3">
                                                            <label class="control-label col-md-12">Bidang Pelaksanaan Pembangunan Desa</label>
                                                        </div>
                                                        <div class="col-md-9">
                                                            <div class="form-group row">
                                                                <label class="control-label col-md-3" style="font-style: italic;">Belanja Pegawai</label>
                                                                <div class="col-md-9">
                                                                    <input type="text" class="form-control" style="text-align: right;" id="belanja_pegawai_pembangunanper" name="belanja_pegawai_pembangunan" value="" maxlength="30" disabled />
                                                                    <span class="form-control-feedback left" aria-hidden="true"> Rp</span>
                                                                </div>
                                                            </div>
                                                            <div class="form-group row">
                                                                <label class="control-label col-md-3" style="font-style: italic;">Belanja Modal</label>
                                                                <div class="col-md-9">
                                                                    <input type="text" class="form-control" style="text-align: right;" id="belanja_modal_pembangunanper" name="belanja_modal_pembangunan" value="" maxlength="30" disabled />
                                                                    <span class="form-control-feedback left" aria-hidden="true"> Rp</span>
                                                                </div>
                                                            </div>
                                                            <div class="form-group row">
                                                                <label class="control-label col-md-3" style="font-style: italic;">Belanja Barang dan Jasa</label>
                                                                <div class="col-md-9">
                                                                    <input type="text" class="form-control" style="text-align: right;" id="belanja_barjas_pembangunanper" name="belanja_barjas_pembangunan" value="" maxlength="30" disabled />
                                                                    <span class="form-control-feedback left" aria-hidden="true"> Rp</span>
                                                                </div>
                                                            </div>
                                                            <div class="form-group row">
                                                                <label class="control-label col-md-3" style="font-style: italic;">Belanja Tak Terduga</label>
                                                                <div class="col-md-9">
                                                                    <input type="text" class="form-control" style="text-align: right;" id="belanja_takterduga_pembangunanper" name="belanja_takterduga_pembangunan" value="" maxlength="30" disabled />
                                                                    <span class="form-control-feedback left" aria-hidden="true"> Rp</span>
                                                                </div>
                                                            </div>
                                                            <div class="form-group row">
                                                                <label class="control-label col-md-3" style="font-style: italic;">Total Belanja</label>
                                                                <div class="col-md-9">
                                                                    <input type="text" class="form-control" style="text-align: right;" id="belanja_total_pembangunanper" name="belanja_total_pembangunan" value="" maxlength="30" readonly />
                                                                    <span class="form-control-feedback left" aria-hidden="true"> Rp</span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="ln_solid" style="border-width: 5px;"></div>
                                                    <div class="row">
                                                        <div class="col-md-3">
                                                            <label class="control-label col-md-12">Bidang Pembinaan Kemasyarakatan</label>
                                                        </div>
                                                        <div class="col-md-9">
                                                            <div class="form-group row">
                                                                <label class="control-label col-md-3" style="font-style: italic;">Belanja Pegawai</label>
                                                                <div class="col-md-9">
                                                                    <input type="text" class="form-control" style="text-align: right;" id="belanja_pegawai_binamasyarakatper" name="belanja_pegawai_binamasyarakat" value="" maxlength="30" disabled />
                                                                    <span class="form-control-feedback left" aria-hidden="true"> Rp</span>
                                                                </div>
                                                            </div>
                                                            <div class="form-group row">
                                                                <label class="control-label col-md-3" style="font-style: italic;">Belanja Modal</label>
                                                                <div class="col-md-9">
                                                                    <input type="text" class="form-control" style="text-align: right;" id="belanja_modal_binamasyarakatper" name="belanja_modal_binamasyarakat" value="" maxlength="30" disabled />
                                                                    <span class="form-control-feedback left" aria-hidden="true"> Rp</span>
                                                                </div>
                                                            </div>
                                                            <div class="form-group row">
                                                                <label class="control-label col-md-3" style="font-style: italic;">Belanja Barang dan Jasa</label>
                                                                <div class="col-md-9">
                                                                    <input type="text" class="form-control" style="text-align: right;" id="belanja_barjas_binamasyarakatper" name="belanja_barjas_binamasyarakat" value="" maxlength="30" disabled />
                                                                    <span class="form-control-feedback left" aria-hidden="true"> Rp</span>
                                                                </div>
                                                            </div>
                                                            <div class="form-group row">
                                                                <label class="control-label col-md-3" style="font-style: italic;">Belanja Tak Terduga</label>
                                                                <div class="col-md-9">
                                                                    <input type="text" class="form-control" style="text-align: right;" id="belanja_takterduga_binamasyarakatper" name="belanja_takterduga_binamasyarakat" value="" maxlength="30" disabled />
                                                                    <span class="form-control-feedback left" aria-hidden="true"> Rp</span>
                                                                </div>
                                                            </div>
                                                            <div class="form-group row">
                                                                <label class="control-label col-md-3" style="font-style: italic;">Total Belanja</label>
                                                                <div class="col-md-9">
                                                                    <input type="text" class="form-control" style="text-align: right;" id="belanja_total_binamasyarakatper" name="belanja_total_binamasyarakat" value="" maxlength="30" readonly />
                                                                    <span class="form-control-feedback left" aria-hidden="true"> Rp</span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="ln_solid" style="border-width: 5px;"></div>
                                                    <div class="row">
                                                        <div class="col-md-3">
                                                            <label class="control-label col-md-12">Bidang Pemberdayaan Masyarakat</label>
                                                        </div>
                                                        <div class="col-md-9">
                                                            <div class="form-group row">
                                                                <label class="control-label col-md-3" style="font-style: italic;">Belanja Pegawai</label>
                                                                <div class="col-md-9">
                                                                    <input type="text" class="form-control" style="text-align: right;" id="belanja_pegawai_berdayamasyarakatper" name="belanja_pegawai_berdayamasyarakat" value="" maxlength="30" disabled />
                                                                    <span class="form-control-feedback left" aria-hidden="true"> Rp</span>
                                                                </div>
                                                            </div>
                                                            <div class="form-group row">
                                                                <label class="control-label col-md-3" style="font-style: italic;">Belanja Modal</label>
                                                                <div class="col-md-9">
                                                                    <input type="text" class="form-control" style="text-align: right;" id="belanja_modal_berdayamasyarakatper" name="belanja_modal_berdayamasyarakat" value="" maxlength="30" disabled />
                                                                    <span class="form-control-feedback left" aria-hidden="true"> Rp</span>
                                                                </div>
                                                            </div>
                                                            <div class="form-group row">
                                                                <label class="control-label col-md-3" style="font-style: italic;">Belanja Barang dan Jasa</label>
                                                                <div class="col-md-9">
                                                                    <input type="text" class="form-control" style="text-align: right;" id="belanja_barjas_berdayamasyarakatper" name="belanja_barjas_berdayamasyarakat" value="" maxlength="30" disabled />
                                                                    <span class="form-control-feedback left" aria-hidden="true"> Rp</span>
                                                                </div>
                                                            </div>
                                                            <div class="form-group row">
                                                                <label class="control-label col-md-3" style="font-style: italic;">Belanja Tak Terduga</label>
                                                                <div class="col-md-9">
                                                                    <input type="text" class="form-control" style="text-align: right;" id="belanja_takterduga_berdayamasyarakatper" name="belanja_takterduga_berdayamasyarakat" value="" maxlength="30" disabled />
                                                                    <span class="form-control-feedback left" aria-hidden="true"> Rp</span>
                                                                </div>
                                                            </div>
                                                            <div class="form-group row">
                                                                <label class="control-label col-md-3" style="font-style: italic;">Total Belanja</label>
                                                                <div class="col-md-9">
                                                                    <input type="text" class="form-control" style="text-align: right;" id="belanja_total_berdayamasyarakatper" name="belanja_total_berdayamasyarakat" value="" maxlength="30" readonly />
                                                                    <span class="form-control-feedback left" aria-hidden="true"> Rp</span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="ln_solid" style="border-width: 5px;"></div>
                                                    <div class="row">
                                                        <div class="col-md-3">
                                                            <label class="control-label col-md-12">Bidang Penanggulangan Bencana Darurat</label>
                                                        </div>
                                                        <div class="col-md-9">
                                                            <div class="form-group row">
                                                                <label class="control-label col-md-3" style="font-style: italic;">Belanja Pegawai</label>
                                                                <div class="col-md-9">
                                                                    <input type="text" class="form-control" style="text-align: right;" id="belanja_pegawai_pbdper" name="belanja_pegawai_pbd" value="" maxlength="30" disabled />
                                                                    <span class="form-control-feedback left" aria-hidden="true"> Rp</span>
                                                                </div>
                                                            </div>
                                                            <div class="form-group row">
                                                                <label class="control-label col-md-3" style="font-style: italic;">Belanja Modal</label>
                                                                <div class="col-md-9">
                                                                    <input type="text" class="form-control" style="text-align: right;" id="belanja_modal_pbdper" name="belanja_modal_pbd" value="" maxlength="30" disabled />
                                                                    <span class="form-control-feedback left" aria-hidden="true"> Rp</span>
                                                                </div>
                                                            </div>
                                                            <div class="form-group row">
                                                                <label class="control-label col-md-3" style="font-style: italic;">Belanja Barang dan Jasa</label>
                                                                <div class="col-md-9">
                                                                    <input type="text" class="form-control" style="text-align: right;" id="belanja_barjas_pbdper" name="belanja_barjas_pbd" value="" maxlength="30" disabled />
                                                                    <span class="form-control-feedback left" aria-hidden="true"> Rp</span>
                                                                </div>
                                                            </div>
                                                            <div class="form-group row">
                                                                <label class="control-label col-md-3" style="font-style: italic;">Belanja Tak Terduga</label>
                                                                <div class="col-md-9">
                                                                    <input type="text" class="form-control" style="text-align: right;" id="belanja_takterduga_pbdper" name="belanja_takterduga_pbd" value="" maxlength="30" disabled />
                                                                    <span class="form-control-feedback left" aria-hidden="true"> Rp</span>
                                                                </div>
                                                            </div>
                                                            <div class="form-group row">
                                                                <label class="control-label col-md-3" style="font-style: italic;">Total Belanja</label>
                                                                <div class="col-md-9">
                                                                    <input type="text" class="form-control" style="text-align: right;" id="belanja_total_pbdper" name="belanja_total_pbd" value="" maxlength="30" readonly />
                                                                    <span class="form-control-feedback left" aria-hidden="true"> Rp</span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="ln_solid"></div>

                                                    <div class="form-group">
                                                        <div class="col-md-12 col-sm-12" style="text-align: center;">
                                                            <button type="reset" class="btn btn-sm" style="background-color: #e5eefe;" id="batal_belanjaperubahan" onclick="batalbelanjaperubahan()" disabled>Batal</button>
                                                            <button type="submit" id="submit_belanjaperubahan" class="btn text-white btn-sm" style="background-color: #49bbc6;" disabled><i class="fa fa-save"></i> Simpan</button>
                                                        </div>
                                                    </div>

                                                </form>

                                                <div class="ln_solid mt-4" style="border-width: 5px;"></div>
                                                <h6 style="text-align: center; font-weight:bold;">Pembiayaan Desa <a type="button" onclick="ubahpembiayaanperubahan()" class="btn text-white btn-sm pull-right" style="background-color: #49bbc6;"><i class="fa fa-edit"></i> Ubah</a></h6>
                                                <div class="ln_solid" style="border-width: 5px;"></div>

                                                <form action="#" method="post" enctype="multipart/form-data" class="form-horizontal mb-lg">
                                                    @csrf
                                                    <div class="form-group row">
                                                        <label class="control-label col-md-3" style="font-style: italic;">Penerimaan Pembiayaan</label>
                                                        <div class="col-md-9">
                                                            <input type="text" class="form-control" style="text-align: right;" id="biaya_penerimaanper" name="penerimaan_biaya" value="" maxlength="30" disabled />
                                                            <span class="form-control-feedback left" aria-hidden="true"> Rp</span>
                                                        </div>
                                                    </div>
                                                    <div class="form-group row">
                                                        <label class="control-label col-md-3" style="font-style: italic;">Pengeluaran Pembiayaan</label>
                                                        <div class="col-md-9">
                                                            <input type="text" class="form-control" style="text-align: right;" id="biaya_pengeluaranper" name="pengeluaran_biaya" value="" maxlength="30" disabled />
                                                            <span class="form-control-feedback left" aria-hidden="true"> Rp</span>
                                                        </div>
                                                    </div>
                                                    <div class="ln_solid"></div>
                                                    <div class="form-group">
                                                        <div class="col-md-12 col-sm-12" style="text-align: center;">
                                                            <button type="reset" class="btn btn-sm" style="background-color: #e5eefe;" id="batal_pembiayaanperubahan" onclick="batalpembiayaanperubahan()" disabled>Batal</button>
                                                            <button type="submit" id="submit_pembiayaanperubahan" class="btn text-white btn-sm" style="background-color: #49bbc6;" disabled><i class="fa fa-save"></i> Simpan</button>
                                                        </div>
                                                    </div>
                                                </form>

                                            </div>
                                        <?php endforeach; ?>

                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>

            </div>

        </div>

    </div>

<!-- /page content -->


<script>
    function ubahpendapatanmurni() {
        $('#pad').removeAttr("disabled");
        $('#dd').removeAttr("disabled");
        $('#alokasi_dd').removeAttr("disabled");
        $('#dbh').removeAttr("disabled");
        $('#pl').removeAttr("disabled");
        $('#submit_pendapatanmurni').removeAttr("disabled");
        $('#batal_pendapatanmurni').removeAttr("disabled");
    }

    function batalpendapatanmurni() {
        $('#pad').attr('disabled', 'disabled');
        $('#dd').attr('disabled', 'disabled');
        $('#alokasi_dd').attr('disabled', 'disabled');
        $('#dbh').attr('disabled', 'disabled');
        $('#pl').attr('disabled', 'disabled');
        $('#submit_pendapatanmurni').attr('disabled', 'disabled');
        $('#batal_pendapatanmurni').attr('disabled', 'disabled');
    }

    function ubahbelanjamurni() {
        $('#belanja_pegawai_pemdes').removeAttr("disabled");
        $('#belanja_modal_pemdes').removeAttr("disabled");
        $('#belanja_barjas_pemdes').removeAttr("disabled");
        $('#belanja_takterduga_pemdes').removeAttr("disabled");
        $('#belanja_pegawai_pembangunan').removeAttr("disabled");
        $('#belanja_modal_pembangunan').removeAttr("disabled");
        $('#belanja_barjas_pembangunan').removeAttr("disabled");
        $('#belanja_takterduga_pembangunan').removeAttr("disabled");
        $('#belanja_pegawai_binamasyarakat').removeAttr("disabled");
        $('#belanja_modal_binamasyarakat').removeAttr("disabled");
        $('#belanja_barjas_binamasyarakat').removeAttr("disabled");
        $('#belanja_takterduga_binamasyarakat').removeAttr("disabled");
        $('#belanja_pegawai_berdayamasyarakat').removeAttr("disabled");
        $('#belanja_modal_berdayamasyarakat').removeAttr("disabled");
        $('#belanja_barjas_berdayamasyarakat').removeAttr("disabled");
        $('#belanja_takterduga_berdayamasyarakat').removeAttr("disabled");
        $('#belanja_pegawai_pbd').removeAttr("disabled");
        $('#belanja_modal_pbd').removeAttr("disabled");
        $('#belanja_barjas_pbd').removeAttr("disabled");
        $('#belanja_takterduga_pbd').removeAttr("disabled");
        $('#submit_belanjamurni').removeAttr("disabled");
        $('#batal_belanjamurni').removeAttr("disabled");
    }

    function batalbelanjamurni() {
        $('#belanja_pegawai_pemdes').attr('disabled', 'disabled');
        $('#belanja_modal_pemdes').attr('disabled', 'disabled');
        $('#belanja_barjas_pemdes').attr('disabled', 'disabled');
        $('#belanja_takterduga_pemdes').attr('disabled', 'disabled');
        $('#belanja_pegawai_pembangunan').attr('disabled', 'disabled');
        $('#belanja_modal_pembangunan').attr('disabled', 'disabled');
        $('#belanja_barjas_pembangunan').attr('disabled', 'disabled');
        $('#belanja_takterduga_pembangunan').attr('disabled', 'disabled');
        $('#belanja_pegawai_binamasyarakat').attr('disabled', 'disabled');
        $('#belanja_modal_binamasyarakat').attr('disabled', 'disabled');
        $('#belanja_barjas_binamasyarakat').attr('disabled', 'disabled');
        $('#belanja_takterduga_binamasyarakat').attr('disabled', 'disabled');
        $('#belanja_pegawai_berdayamasyarakat').attr('disabled', 'disabled');
        $('#belanja_modal_berdayamasyarakat').attr('disabled', 'disabled');
        $('#belanja_barjas_berdayamasyarakat').attr('disabled', 'disabled');
        $('#belanja_takterduga_berdayamasyarakat').attr('disabled', 'disabled');
        $('#belanja_pegawai_pbd').attr('disabled', 'disabled');
        $('#belanja_modal_pbd').attr('disabled', 'disabled');
        $('#belanja_barjas_pbd').attr('disabled', 'disabled');
        $('#belanja_takterduga_pbd').attr('disabled', 'disabled');
        $('#submit_belanjamurni').attr('disabled', 'disabled');
        $('#batal_belanjamurni').attr('disabled', 'disabled');
    }

    function ubahpembiayaanmurni() {
        $('#penerimaan_biaya').removeAttr("disabled");
        $('#pengeluaran_biaya').removeAttr("disabled");
        $('#submit_pembiayaanmurni').removeAttr("disabled");
        $('#batal_pembiayaanmurni').removeAttr("disabled");
    }

    function batalpembiayaanmurni() {
        $('#penerimaan_biaya').attr('disabled', 'disabled');
        $('#pengeluaran_biaya').attr('disabled', 'disabled');
        $('#submit_pembiayaanmurni').attr('disabled', 'disabled');
        $('#batal_pembiayaanmurni').attr('disabled', 'disabled');
    }

    function ubahpendapatanperubahan() {
        $('#padper').removeAttr("disabled");
        $('#ddper').removeAttr("disabled");
        $('#alokasi_ddper').removeAttr("disabled");
        $('#dbhper').removeAttr("disabled");
        $('#plper').removeAttr("disabled");
        $('#submit_pendapatanperubahan').removeAttr("disabled");
        $('#batal_pendapatanperubahan').removeAttr("disabled");
    }

    function batalpendapatanperubahan() {
        $('#padper').attr('disabled', 'disabled');
        $('#ddper').attr('disabled', 'disabled');
        $('#alokasi_ddper').attr('disabled', 'disabled');
        $('#dbhper').attr('disabled', 'disabled');
        $('#plper').attr('disabled', 'disabled');
        $('#submit_pendapatanperubahan').attr('disabled', 'disabled');
        $('#batal_pendapatanperubahan').attr('disabled', 'disabled');
    }

    function ubahbelanjaperubahan() {
        $('#belanja_pegawai_pemdesper').removeAttr("disabled");
        $('#belanja_modal_pemdesper').removeAttr("disabled");
        $('#belanja_barjas_pemdesper').removeAttr("disabled");
        $('#belanja_takterduga_pemdesper').removeAttr("disabled");
        $('#belanja_pegawai_pembangunanper').removeAttr("disabled");
        $('#belanja_modal_pembangunanper').removeAttr("disabled");
        $('#belanja_barjas_pembangunanper').removeAttr("disabled");
        $('#belanja_takterduga_pembangunanper').removeAttr("disabled");
        $('#belanja_pegawai_binamasyarakatper').removeAttr("disabled");
        $('#belanja_modal_binamasyarakatper').removeAttr("disabled");
        $('#belanja_barjas_binamasyarakatper').removeAttr("disabled");
        $('#belanja_takterduga_binamasyarakatper').removeAttr("disabled");
        $('#belanja_pegawai_berdayamasyarakatper').removeAttr("disabled");
        $('#belanja_modal_berdayamasyarakatper').removeAttr("disabled");
        $('#belanja_barjas_berdayamasyarakatper').removeAttr("disabled");
        $('#belanja_takterduga_berdayamasyarakatper').removeAttr("disabled");
        $('#belanja_pegawai_pbdper').removeAttr("disabled");
        $('#belanja_modal_pbdper').removeAttr("disabled");
        $('#belanja_barjas_pbdper').removeAttr("disabled");
        $('#belanja_takterduga_pbdper').removeAttr("disabled");
        $('#submit_belanjaperubahan').removeAttr("disabled");
        $('#batal_belanjaperubahan').removeAttr("disabled");
    }

    function batalbelanjaperubahan() {
        $('#belanja_pegawai_pemdesper').attr('disabled', 'disabled');
        $('#belanja_modal_pemdesper').attr('disabled', 'disabled');
        $('#belanja_barjas_pemdesper').attr('disabled', 'disabled');
        $('#belanja_takterduga_pemdesper').attr('disabled', 'disabled');
        $('#belanja_pegawai_pembangunanper').attr('disabled', 'disabled');
        $('#belanja_modal_pembangunanper').attr('disabled', 'disabled');
        $('#belanja_barjas_pembangunanper').attr('disabled', 'disabled');
        $('#belanja_takterduga_pembangunanper').attr('disabled', 'disabled');
        $('#belanja_pegawai_binamasyarakatper').attr('disabled', 'disabled');
        $('#belanja_modal_binamasyarakatper').attr('disabled', 'disabled');
        $('#belanja_barjas_binamasyarakatper').attr('disabled', 'disabled');
        $('#belanja_takterduga_binamasyarakatper').attr('disabled', 'disabled');
        $('#belanja_pegawai_berdayamasyarakatper').attr('disabled', 'disabled');
        $('#belanja_modal_berdayamasyarakatper').attr('disabled', 'disabled');
        $('#belanja_barjas_berdayamasyarakatper').attr('disabled', 'disabled');
        $('#belanja_takterduga_berdayamasyarakatper').attr('disabled', 'disabled');
        $('#belanja_pegawai_pbdper').attr('disabled', 'disabled');
        $('#belanja_modal_pbdper').attr('disabled', 'disabled');
        $('#belanja_barjas_pbdper').attr('disabled', 'disabled');
        $('#belanja_takterduga_pbdper').attr('disabled', 'disabled');
        $('#submit_belanjaperubahan').attr('disabled', 'disabled');
        $('#batal_belanjaperubahan').attr('disabled', 'disabled');
    }

    function ubahpembiayaanperubahan() {
        $('#biaya_penerimaanper').removeAttr("disabled");
        $('#biaya_pengeluaranper').removeAttr("disabled");
        $('#submit_pembiayaanperubahan').removeAttr("disabled");
        $('#batal_pembiayaanperubahan').removeAttr("disabled");
    }

    function batalpembiayaanperubahan() {
        $('#biaya_penerimaanper').attr('disabled', 'disabled');
        $('#biaya_pengeluaranper').attr('disabled', 'disabled');
        $('#submit_pembiayaanperubahan').attr('disabled', 'disabled');
        $('#batal_pembiayaanperubahan').attr('disabled', 'disabled');
    }
</script>

<script type="text/javascript">
    var pad = document.getElementById('pad');
    pad.addEventListener('keyup', function(e) {
        pad.value = formatRupiah(this.value);
    });

    var dd = document.getElementById('dd');
    dd.addEventListener('keyup', function(e) {
        dd.value = formatRupiah(this.value);
    });

    var alokasi_dd = document.getElementById('alokasi_dd');
    alokasi_dd.addEventListener('keyup', function(e) {
        alokasi_dd.value = formatRupiah(this.value);
    });

    var dbh = document.getElementById('dbh');
    dbh.addEventListener('keyup', function(e) {
        dbh.value = formatRupiah(this.value);
    });

    var pl = document.getElementById('pl');
    pl.addEventListener('keyup', function(e) {
        pl.value = formatRupiah(this.value);
    });

    var padper = document.getElementById('padper');
    padper.addEventListener('keyup', function(e) {
        padper.value = formatRupiah(this.value);
    });

    var ddper = document.getElementById('ddper');
    ddper.addEventListener('keyup', function(e) {
        ddper.value = formatRupiah(this.value);
    });

    var alokasi_ddper = document.getElementById('alokasi_ddper');
    alokasi_ddper.addEventListener('keyup', function(e) {
        alokasi_ddper.value = formatRupiah(this.value);
    });

    var dbhper = document.getElementById('dbhper');
    dbhper.addEventListener('keyup', function(e) {
        dbhper.value = formatRupiah(this.value);
    });

    var plper = document.getElementById('plper');
    plper.addEventListener('keyup', function(e) {
        plper.value = formatRupiah(this.value);
    });

    var belanja_pegawai_pemdes = document.getElementById('belanja_pegawai_pemdes');
    belanja_pegawai_pemdes.addEventListener('keyup', function(e) {
        belanja_pegawai_pemdes.value = formatRupiah(this.value);
    });
    var belanja_modal_pemdes = document.getElementById('belanja_modal_pemdes');
    belanja_modal_pemdes.addEventListener('keyup', function(e) {
        belanja_modal_pemdes.value = formatRupiah(this.value);
    });
    var belanja_barjas_pemdes = document.getElementById('belanja_barjas_pemdes');
    belanja_barjas_pemdes.addEventListener('keyup', function(e) {
        belanja_barjas_pemdes.value = formatRupiah(this.value);
    });
    var belanja_takterduga_pemdes = document.getElementById('belanja_takterduga_pemdes');
    belanja_takterduga_pemdes.addEventListener('keyup', function(e) {
        belanja_takterduga_pemdes.value = formatRupiah(this.value);
    });
    var belanja_total_pemdes = document.getElementById('belanja_total_pemdes');
    belanja_total_pemdes.addEventListener('keyup', function(e) {
        belanja_total_pemdes.value = formatRupiah(this.value);
    });
    var belanja_pegawai_pembangunan = document.getElementById('belanja_pegawai_pembangunan');
    belanja_pegawai_pembangunan.addEventListener('keyup', function(e) {
        belanja_pegawai_pembangunan.value = formatRupiah(this.value);
    });
    var belanja_modal_pembangunan = document.getElementById('belanja_modal_pembangunan');
    belanja_modal_pembangunan.addEventListener('keyup', function(e) {
        belanja_modal_pembangunan.value = formatRupiah(this.value);
    });
    var belanja_barjas_pembangunan = document.getElementById('belanja_barjas_pembangunan');
    belanja_barjas_pembangunan.addEventListener('keyup', function(e) {
        belanja_barjas_pembangunan.value = formatRupiah(this.value);
    });
    var belanja_takterduga_pembangunan = document.getElementById('belanja_takterduga_pembangunan');
    belanja_takterduga_pembangunan.addEventListener('keyup', function(e) {
        belanja_takterduga_pembangunan.value = formatRupiah(this.value);
    });
    var belanja_total_pembangunan = document.getElementById('belanja_total_pembangunan');
    belanja_total_pembangunan.addEventListener('keyup', function(e) {
        belanja_total_pembangunan.value = formatRupiah(this.value);
    });
    var belanja_pegawai_binamasyarakat = document.getElementById('belanja_pegawai_binamasyarakat');
    belanja_pegawai_binamasyarakat.addEventListener('keyup', function(e) {
        belanja_pegawai_binamasyarakat.value = formatRupiah(this.value);
    });
    var belanja_modal_binamasyarakat = document.getElementById('belanja_modal_binamasyarakat');
    belanja_modal_binamasyarakat.addEventListener('keyup', function(e) {
        belanja_modal_binamasyarakat.value = formatRupiah(this.value);
    });
    var belanja_barjas_binamasyarakat = document.getElementById('belanja_barjas_binamasyarakat');
    belanja_barjas_binamasyarakat.addEventListener('keyup', function(e) {
        belanja_barjas_binamasyarakat.value = formatRupiah(this.value);
    });
    var belanja_takterduga_binamasyarakat = document.getElementById('belanja_takterduga_binamasyarakat');
    belanja_takterduga_binamasyarakat.addEventListener('keyup', function(e) {
        belanja_takterduga_binamasyarakat.value = formatRupiah(this.value);
    });
    var belanja_total_binamasyarakat = document.getElementById('belanja_total_binamasyarakat');
    belanja_total_binamasyarakat.addEventListener('keyup', function(e) {
        belanja_total_binamasyarakat.value = formatRupiah(this.value);
    });
    var belanja_pegawai_berdayamasyarakat = document.getElementById('belanja_pegawai_berdayamasyarakat');
    belanja_pegawai_berdayamasyarakat.addEventListener('keyup', function(e) {
        belanja_pegawai_berdayamasyarakat.value = formatRupiah(this.value);
    });
    var belanja_modal_berdayamasyarakat = document.getElementById('belanja_modal_berdayamasyarakat');
    belanja_modal_berdayamasyarakat.addEventListener('keyup', function(e) {
        belanja_modal_berdayamasyarakat.value = formatRupiah(this.value);
    });
    var belanja_barjas_berdayamasyarakat = document.getElementById('belanja_barjas_berdayamasyarakat');
    belanja_barjas_berdayamasyarakat.addEventListener('keyup', function(e) {
        belanja_barjas_berdayamasyarakat.value = formatRupiah(this.value);
    });
    var belanja_takterduga_berdayamasyarakat = document.getElementById('belanja_takterduga_berdayamasyarakat');
    belanja_takterduga_berdayamasyarakat.addEventListener('keyup', function(e) {
        belanja_takterduga_berdayamasyarakat.value = formatRupiah(this.value);
    });
    var belanja_total_berdayamasyarakat = document.getElementById('belanja_total_berdayamasyarakat');
    belanja_total_berdayamasyarakat.addEventListener('keyup', function(e) {
        belanja_total_berdayamasyarakat.value = formatRupiah(this.value);
    });
    var belanja_pegawai_pbd = document.getElementById('belanja_pegawai_pbd');
    belanja_pegawai_pbd.addEventListener('keyup', function(e) {
        belanja_pegawai_pbd.value = formatRupiah(this.value);
    });
    var belanja_modal_pbd = document.getElementById('belanja_modal_pbd');
    belanja_modal_pbd.addEventListener('keyup', function(e) {
        belanja_modal_pbd.value = formatRupiah(this.value);
    });
    var belanja_barjas_pbd = document.getElementById('belanja_barjas_pbd');
    belanja_barjas_pbd.addEventListener('keyup', function(e) {
        belanja_barjas_pbd.value = formatRupiah(this.value);
    });
    var belanja_takterduga_pbd = document.getElementById('belanja_takterduga_pbd');
    belanja_takterduga_pbd.addEventListener('keyup', function(e) {
        belanja_takterduga_pbd.value = formatRupiah(this.value);
    });
    var belanja_total_pbd = document.getElementById('belanja_total_pbd');
    belanja_total_pbd.addEventListener('keyup', function(e) {
        belanja_total_pbd.value = formatRupiah(this.value);
    });

    var penerimaan_biaya = document.getElementById('penerimaan_biaya');
    penerimaan_biaya.addEventListener('keyup', function(e) {
        penerimaan_biaya.value = formatRupiah(this.value);
    });
    var pengeluaran_biaya = document.getElementById('pengeluaran_biaya');
    pengeluaran_biaya.addEventListener('keyup', function(e) {
        pengeluaran_biaya.value = formatRupiah(this.value);
    });



    var belanja_pegawai_pemdesper = document.getElementById('belanja_pegawai_pemdesper');
    belanja_pegawai_pemdesper.addEventListener('keyup', function(e) {
        belanja_pegawai_pemdesper.value = formatRupiah(this.value);
    });
    var belanja_modal_pemdesper = document.getElementById('belanja_modal_pemdesper');
    belanja_modal_pemdesper.addEventListener('keyup', function(e) {
        belanja_modal_pemdesper.value = formatRupiah(this.value);
    });
    var belanja_barjas_pemdesper = document.getElementById('belanja_barjas_pemdesper');
    belanja_barjas_pemdesper.addEventListener('keyup', function(e) {
        belanja_barjas_pemdesper.value = formatRupiah(this.value);
    });
    var belanja_takterduga_pemdesper = document.getElementById('belanja_takterduga_pemdesper');
    belanja_takterduga_pemdesper.addEventListener('keyup', function(e) {
        belanja_takterduga_pemdesper.value = formatRupiah(this.value);
    });
    var belanja_total_pemdesper = document.getElementById('belanja_total_pemdesper');
    belanja_total_pemdesper.addEventListener('keyup', function(e) {
        belanja_total_pemdesper.value = formatRupiah(this.value);
    });
    var belanja_pegawai_pembangunanper = document.getElementById('belanja_pegawai_pembangunanper');
    belanja_pegawai_pembangunanper.addEventListener('keyup', function(e) {
        belanja_pegawai_pembangunanper.value = formatRupiah(this.value);
    });
    var belanja_modal_pembangunanper = document.getElementById('belanja_modal_pembangunanper');
    belanja_modal_pembangunanper.addEventListener('keyup', function(e) {
        belanja_modal_pembangunanper.value = formatRupiah(this.value);
    });
    var belanja_barjas_pembangunanper = document.getElementById('belanja_barjas_pembangunanper');
    belanja_barjas_pembangunanper.addEventListener('keyup', function(e) {
        belanja_barjas_pembangunanper.value = formatRupiah(this.value);
    });
    var belanja_takterduga_pembangunanper = document.getElementById('belanja_takterduga_pembangunanper');
    belanja_takterduga_pembangunanper.addEventListener('keyup', function(e) {
        belanja_takterduga_pembangunanper.value = formatRupiah(this.value);
    });
    var belanja_total_pembangunanper = document.getElementById('belanja_total_pembangunanper');
    belanja_total_pembangunanper.addEventListener('keyup', function(e) {
        belanja_total_pembangunanper.value = formatRupiah(this.value);
    });
    var belanja_pegawai_binamasyarakatper = document.getElementById('belanja_pegawai_binamasyarakatper');
    belanja_pegawai_binamasyarakatper.addEventListener('keyup', function(e) {
        belanja_pegawai_binamasyarakatper.value = formatRupiah(this.value);
    });
    var belanja_modal_binamasyarakatper = document.getElementById('belanja_modal_binamasyarakatper');
    belanja_modal_binamasyarakatper.addEventListener('keyup', function(e) {
        belanja_modal_binamasyarakatper.value = formatRupiah(this.value);
    });
    var belanja_barjas_binamasyarakatper = document.getElementById('belanja_barjas_binamasyarakatper');
    belanja_barjas_binamasyarakatper.addEventListener('keyup', function(e) {
        belanja_barjas_binamasyarakatper.value = formatRupiah(this.value);
    });
    var belanja_takterduga_binamasyarakatper = document.getElementById('belanja_takterduga_binamasyarakatper');
    belanja_takterduga_binamasyarakatper.addEventListener('keyup', function(e) {
        belanja_takterduga_binamasyarakatper.value = formatRupiah(this.value);
    });
    var belanja_total_binamasyarakatper = document.getElementById('belanja_total_binamasyarakatper');
    belanja_total_binamasyarakatper.addEventListener('keyup', function(e) {
        belanja_total_binamasyarakatper.value = formatRupiah(this.value);
    });
    var belanja_pegawai_berdayamasyarakatper = document.getElementById('belanja_pegawai_berdayamasyarakatper');
    belanja_pegawai_berdayamasyarakatper.addEventListener('keyup', function(e) {
        belanja_pegawai_berdayamasyarakatper.value = formatRupiah(this.value);
    });
    var belanja_modal_berdayamasyarakatper = document.getElementById('belanja_modal_berdayamasyarakatper');
    belanja_modal_berdayamasyarakatper.addEventListener('keyup', function(e) {
        belanja_modal_berdayamasyarakatper.value = formatRupiah(this.value);
    });
    var belanja_barjas_berdayamasyarakatper = document.getElementById('belanja_barjas_berdayamasyarakatper');
    belanja_barjas_berdayamasyarakatper.addEventListener('keyup', function(e) {
        belanja_barjas_berdayamasyarakatper.value = formatRupiah(this.value);
    });
    var belanja_takterduga_berdayamasyarakatper = document.getElementById('belanja_takterduga_berdayamasyarakatper');
    belanja_takterduga_berdayamasyarakatper.addEventListener('keyup', function(e) {
        belanja_takterduga_berdayamasyarakatper.value = formatRupiah(this.value);
    });
    var belanja_total_berdayamasyarakatper = document.getElementById('belanja_total_berdayamasyarakatper');
    belanja_total_berdayamasyarakatper.addEventListener('keyup', function(e) {
        belanja_total_berdayamasyarakatper.value = formatRupiah(this.value);
    });
    var belanja_pegawai_pbdper = document.getElementById('belanja_pegawai_pbdper');
    belanja_pegawai_pbdper.addEventListener('keyup', function(e) {
        belanja_pegawai_pbdper.value = formatRupiah(this.value);
    });
    var belanja_modal_pbdper = document.getElementById('belanja_modal_pbdper');
    belanja_modal_pbdper.addEventListener('keyup', function(e) {
        belanja_modal_pbdper.value = formatRupiah(this.value);
    });
    var belanja_barjas_pbdper = document.getElementById('belanja_barjas_pbdper');
    belanja_barjas_pbdper.addEventListener('keyup', function(e) {
        belanja_barjas_pbdper.value = formatRupiah(this.value);
    });
    var belanja_takterduga_pbdper = document.getElementById('belanja_takterduga_pbdper');
    belanja_takterduga_pbdper.addEventListener('keyup', function(e) {
        belanja_takterduga_pbdper.value = formatRupiah(this.value);
    });
    var belanja_total_pbdper = document.getElementById('belanja_total_pbdper');
    belanja_total_pbdper.addEventListener('keyup', function(e) {
        belanja_total_pbdper.value = formatRupiah(this.value);
    });

    var biaya_penerimaanper = document.getElementById('biaya_penerimaanper');
    biaya_penerimaanper.addEventListener('keyup', function(e) {
        biaya_penerimaanper.value = formatRupiah(this.value);
    });
    var biaya_pengeluaranper = document.getElementById('biaya_pengeluaranper');
    biaya_pengeluaranper.addEventListener('keyup', function(e) {
        biaya_pengeluaranper.value = formatRupiah(this.value);
    });

    /* Fungsi formatRupiah */
    function formatRupiah(angka, prefix) {
        var number_string = angka.replace(/[^,\d]/g, '').toString(),
            split = number_string.split(','),
            sisa = split[0].length % 3,
            rupiah = split[0].substr(0, sisa),
            ribuan = split[0].substr(sisa).match(/\d{3}/gi);

        // tambahkan titik jika yang di input sudah menjadi angka ribuan
        if (ribuan) {
            separator = sisa ? '.' : '';
            rupiah += separator + ribuan.join('.');
        }

        rupiah = split[1] != undefined ? rupiah + ',' + split[1] : rupiah;
        return prefix == undefined ? rupiah : (rupiah ? rupiah : '');
    }
</script>

<script>
    $(document).ready(function() {
        $('a[data-toggle="tab"]').on('show.bs.tab', function(e) {
            localStorage.setItem('activeTab', $(e.target).attr('href'));
        });
        var activeTab = localStorage.getItem('activeTab');
        if (activeTab) {
            $('#myTab a[href="' + activeTab + '"]').tab('show');
        }
    });
</script>



@endsection