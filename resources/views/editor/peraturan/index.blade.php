@extends('editor.templates.main')

@section('content')
    <br>
    <br>
    <div class="container mt-5">
        <div class="row">
            <div class="col-md-12">
                <div class="card border-0 shadow rounded">
                    <div class="card-body">
                        <a href="{{ route('peraturan.create') }}" class="btn btn-md btn-success mb-3">TAMBAH PERATURAN</a>
                        <table class="table table-bordered">
                            <thead>
                              <tr>
                                <th scope="col">DOKUMEN</th>
                                <th scope="col">JUDUL</th>
                                <th scope="col">NOMOR</th>
                                <th scope="col">TAHUN</th>
                                <th scope="col">AKSI</th>
                              </tr>
                            </thead>
                            <tbody>
                              @forelse ($peraturans as $peraturan)
                                <tr>
                                    <td class="text-center">
                                        <a href="storage/peraturans/{{ $peraturan->image }}" target="_blank">Unduh Peraturan</a>
                                    </td>
                                    <td>{{ $peraturan->title }}</td>
                                    <td>{{ $peraturan->nomor }}</td>
                                    <td>{{ $peraturan->tahun }}</td>
                                    <td class="text-center">
                                        <form onsubmit="return confirm('Apakah Anda Yakin ?');" action="{{ route('peraturan.destroy', $peraturan->id) }}" method="POST">
                                            <a href="{{ route('peraturan.edit', $peraturan->id) }}" class="btn btn-sm btn-primary">EDIT</a>
                                            @csrf
                                            @method('DELETE')
                                            <button type="submit" class="btn btn-sm btn-danger">HAPUS</button>
                                        </form>
                                        
                                    </td>
                                </tr>
                              @empty
                                  <div class="alert alert-danger">
                                      Data Blog belum Tersedia.
                                  </div>
                              @endforelse
                            </tbody>
                          </table>  
                    </div>
                    <div class="footer">
                        {{ $peraturans->links() }}
                        </div>
                </div>
                
                
            </div>
        </div>
    </div>


    <script>
        //message with toastr
        @if(session()-> has('success'))
        
            toastr.success('{{ session('success') }}', 'BERHASIL!'); 

        @elseif(session()-> has('error'))

            toastr.error('{{ session('error') }}', 'GAGAL!'); 
            
        @endif
    </script>

</body>

@endsection