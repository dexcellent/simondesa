<footer>
    <div class="pull-right">
        <a href="https://inspektorat.lampungutarakab.go.id">By : Inspektorat Kab. Lampung Utara</a>
    </div>
    <div class="clearfix"></div>
</footer>

</div>
</div>

<!-- jQuery -->
<script src="/vendors/jquery/dist/jquery.js"></script>

<!-- Bootstrap -->
<script src="/vendors/bootstrap/dist/js/bootstrap.bundle.min.js"></script>

<script src="/vendors/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="/vendors/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
<script src="/vendors/datatables.net-buttons/js/dataTables.buttons.min.js"></script>
<script src="/vendors/datatables.net-buttons-bs/js/buttons.bootstrap.min.js"></script>
<script src="/vendors/datatables.net-buttons/js/buttons.flash.min.js"></script>
<script src="/vendors/datatables.net-buttons/js/buttons.html5.min.js"></script>
<script src="/vendors/datatables.net-buttons/js/buttons.print.min.js"></script>
<script src="/vendors/datatables.net-fixedheader/js/dataTables.fixedHeader.min.js"></script>
<script src="/vendors/datatables.net-keytable/js/dataTables.keyTable.min.js"></script>
<script src="/vendors/datatables.net-responsive/js/dataTables.responsive.min.js"></script>
<script src="/vendors/datatables.net-responsive-bs/js/responsive.bootstrap.js"></script>
<script src="/vendors/datatables.net-scroller/js/dataTables.scroller.min.js"></script>

<!-- jQuery custom content scroller -->
<script src="/vendors/malihu-custom-scrollbar-plugin/jquery.mCustomScrollbar.concat.min.js"></script>

<!-- Custom Theme Scripts -->
<script src="/build/js/custom.js"></script>

@stack('script')

<script>
    $(document).ready( function () {
        $('.foto-pegawai').click(function(e){
                    var path = $(this).attr('href');
                    var tr = $(this).parents('tr');
                    var nampeg = tr.find('.nampeg').text();
                        nampeg = nampeg.split("NIP.");
                    e.preventDefault();
                        Swal.fire({
                            title: 'E-ABSENSI LU',
                            text: 'CEK FOTO : '+nampeg[0],
                            imageUrl: path,
                            imageWidth: 400,
                            imageHeight: 550,
                            imageAlt: 'Custom image',
                         })
            });

        $('#table1').DataTable();
        $('#tabel_pg').DataTable();
        $('#tableaja').DataTable();

} );
</script>


</body>

</html>