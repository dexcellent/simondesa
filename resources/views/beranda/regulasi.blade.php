@extends('layouts.main')
@section('css')
<!-- Material Icons -->
<link rel="stylesheet" type="text/css"
    href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700,900|Roboto+Slab:400,700" />
<!-- CSS Files -->
<script src="https://kit.fontawesome.com/42d5adcbca.js" crossorigin="anonymous"></script>
<!-- Material Icons -->
<link href="https://fonts.googleapis.com/icon?family=Material+Icons+Round" rel="stylesheet">
<link id="pagestyle" href="/assets/css/material-kit.css?v=3.0.4" rel="stylesheet" />

@endsection
@section('content')
<div role="main" class="main">

    <section class="page-header page-header-modern bg-color-light-scale-1 page-header-md">
        <div class="container">
            <div class="row">
                <div class="col-md-12 align-self-center p-static order-2 text-center">
                    <h1 class="text-dark font-weight-bold text-8">Regulasi Desa</h1>
                    <span class="sub-title text-dark">Regulasi yang berlaku pada Pemerintahan Desa</span>
                </div>
            </div>
        </div>
    </section>

    <div class="container py-4">
        <div class="row">
            <div class="col">
                <div class="card-box table-responsive">
                    <table id="example" class="table table-striped table-bordered nowrap" style="width:100%">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th style="text-align: center">Nama Peraturan</th>
                                <th style="text-align: center">Opsi</th>
                            </tr>
                        </thead>
                        <tbody> 
                            
                            <?php $no = 1; ?>
                            @forelse ($peraturans as $peraturan)                                               
                            <tr>
                                <td style="width:5%; text-align: center;r"><?php echo $no; ?></td>
                                <td style="text-align: left; width:75%">{{ $peraturan->title }}</td>
                                <td class="text-center" style="width:20%">
                                    <a href="storage/peraturans/{{ $peraturan->image }}" target="_blank">Unduh Peraturan</a>
                                </td>
                            </tr>
                            <?php $no++; ?>
                            @empty
                                <div class="alert alert-danger">
                                    Data Blog belum Tersedia.
                                </div>
                            @endforelse									
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

</div>
</div>

@endsection