@if(session()->has('loggedAdmin'))
<script>
    document.location.href='/';
</script>
@endif

@extends('layouts.main')
@section('css')
<script src="/package/dist/sweetalert2.min.js"></script>
<link rel="stylesheet" href="/package/dist/sweetalert2.min.css">

<style>
    .card { background-color: rgba(255, 255, 255, 0.8); }
    .card-header, .card-footer { opacity: 1}
</style>
@endsection
@section('content')
@yield('css')

<div class="page-header overlay overlay-color-dark overlay-show overlay-op-5" style="background-image: url('/img/foto-desa.jpg'); background-repeat: no-repeat; background-size: 100% 100%; height: 600px;" loading="lazy">    
    <div class="container">
        <div class="row">
            <div class="col-lg-4 col-md-12 col-12 mx-auto">
                <div class="card">
                    <div class="card-header p-0 position-relative">
                        <div class="bg-color-primary py-3 pe-1">
                            <h4 class="text-white font-weight-bolder text-center mt-2 mb-0">SIMONDES</h4>
                            <h6 class="text-white font-weight-bolder text-center mt-1 mb-0">LOGIN</h6>
                        </div>
                    </div>                   

                    <div class="card-body">
                        <form role="form" class="text-start" action="/masuk" method="post">
                            @csrf
                            <div class="input-group input-group-outline mt-3 mb-0">

                                <input type="text" class="form-control " placeholder="Username" name="username"
                                    value="{{ old('username') }}">

                            </div>
                            @error('username')
                            <div><small class="text-danger mb-3">{{ $message }}</small></div>
                            @enderror

                            <div class="input-group input-group-outline mt-3 mb-0">

                                <input type="password" class="form-control" placeholder="Password" name="password">
                            </div>
                            @error('password')
                            <div><small class="text-danger mb-3">{{ $message }}</small></div>
                            @enderror
                            <div class="text-center">
                                <button type="submit" class="btn btn-rounded btn-primary w-100 my-4 mb-2">Login </button>
                            </div>
                            <p class="mt-4 text-sm text-center">
                                Belum ada akun? <a href="/register">Registrasi sekarang</a>
                            </p>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@if(session()->has('fail'))
<script>
    Swal.fire({
    position: 'center',
    icon: 'error',
    title:  "{{ session('fail') }}",
    showConfirmButton: true,

    });
</script>


@endif


@endsection