<?php

namespace App\Http\Controllers;

use App\Models\Peraturan;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use App\Models\Admin;
use Illuminate\Support\Facades\DB;


class PeraturanController extends Controller
{
    /**
     * index
     *
     * @return void
     */
    public function index()
    {
        $peraturans = Peraturan::latest()->paginate(5);
        $infos = Admin::where('id', session('loggedEditor'))->first();
        return view('editor.peraturan.index',  [
            'infos' => $infos,
            'peraturans' => $peraturans
        ]);
    }

    /**
     * create
     *
     * @return void
     */
    public function create()
    {
        return view('editor.peraturan.create');
    }

    /**
     * store
     *
     * @param  mixed $request
     * @return void
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'image'     => 'required|mimes:pdf,jpg,jpeg',
            'title'     => 'required',
            'nomor'     => 'max:3',
            'tahun'     => 'max:4',
            'content'   => 'required'
        ]);

        //upload image
        $image = $request->file('image');
        $image->storeAs('peraturans', $image->hashName());

        $peraturan = Peraturan::create([
            'image'     => $image->hashName(),
            'title'     => $request->title,
            'nomor'     => $request->nomor,
            'tahun'     => $request->tahun,
            'content'   => $request->content
        ]);

        if ($peraturan) {
            //redirect dengan pesan sukses
            return redirect()->route('peraturan.index')->with(['success' => 'Data Berhasil Disimpan!']);
        } else {
            //redirect dengan pesan error
            return redirect()->route('peraturan.index')->with(['error' => 'Data Gagal Disimpan!']);
        }
    }

    /**
     * edit
     *
     * @param  mixed $blog
     * @return void
     */
    public function edit(Peraturan $peraturan)
    {
        return view('editor.peraturan.edit', compact('peraturan'));
    }

    /**
     * update
     *
     * @param  mixed $request
     * @param  mixed $blog
     * @return void
     */
    public function update(Request $request, Peraturan $peraturan)
    {
        $this->validate($request, [
            'title'     => 'required',
            'nomor'     => 'max:3',
            'tahun'     => 'max:4',
            'content'   => 'required'
        ]);

        //get data Blog by ID
        $peraturan = Peraturan::findOrFail($peraturan->id);

        if ($request->file('image') == "") {

            $peraturan->update([
                'title'     => $request->title,
                'nomor'     => $request->nomor,
                'tahun'     => $request->tahun,
                'content'   => $request->content
            ]);
        } else {

            //hapus old image
            Storage::disk('local')->delete('public/peraturans/' . $peraturan->image);

            //upload new image
            $image = $request->file('image');
            $image->storeAs('peraturans', $image->hashName());

            $peraturan->update([
                'image'     => $image->hashName(),
                'title'     => $request->title,
                'nomor'     => $request->nomor,
                'tahun'     => $request->tahun,
                'content'   => $request->content
            ]);
        }

        if ($peraturan) {
            //redirect dengan pesan sukses
            return redirect()->route('peraturan.index')->with(['success' => 'Data Berhasil Diupdate!']);
        } else {
            //redirect dengan pesan error
            return redirect()->route('peraturan.index')->with(['error' => 'Data Gagal Diupdate!']);
        }
    }

    /**
     * destroy
     *
     * @param  mixed $id
     * @return void
     */
    public function destroy($id)
    {
        $peraturan = Peraturan::findOrFail($id);
        $peraturan->delete();

        if ($peraturan) {
            //redirect dengan pesan sukses
            return redirect()->route('peraturan.index')->with(['success' => 'Data Berhasil Dihapus!']);
        } else {
            //redirect dengan pesan error
            return redirect()->route('peraturan.index')->with(['error' => 'Data Gagal Dihapus!']);
        }
    }
}
