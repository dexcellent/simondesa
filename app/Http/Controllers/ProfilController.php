<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\Models\Asal;


class ProfilController extends Controller
{

    public function index()
    {
        $data['kecamatans'] = DB::table('asals')
            ->distinct()
            ->where('kecamatan', '!=', '')
            ->get('kecamatan');

        $data['desas'] = DB::table('asals')
            ->where('kecamatan', '!=', '')
            ->get();

        return view('beranda.profil', $data);
    }

    public function lihatprofil(Request $request)
    {
        $data['desas'] =  DB::table('asals')
            ->where('id', $request->desa)
            ->get();
        return view('beranda.profil.viewprofil', $data);
    }

    public function getDesa(Request $request)
    {
        $kecamatan = $request->post('kecamatan');
        $desa = DB::table('asals')->where('kecamatan', $kecamatan)->get();
        $html = '<option value="">---Pilih Desa---</option>';
        foreach ($desa as $ds) {
            $html .= '<option value="' . $ds->id . '">' . $ds->asal . '</option>';
        }
        echo $html;
    }

    // public function pilihdesa(Request $request)
    // {
    //     $desas = DB::table('asals')
    //         ->where('kecamatan', $request->kecamatan)
    //         ->get();
    //     return view('beranda.profil.desa', ['desas' => $desas]);
    // }
}
