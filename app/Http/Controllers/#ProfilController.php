<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\Models\Asal;


class ProfilController extends Controller
{

    public function index()
    {
        $kecamatan = DB::table('asals')
            ->distinct()
            ->where('kecamatan', '!=', '')
            ->get('kecamatan');
        $desa = DB::table('asals')
            ->where('kecamatan', '!=', '')
            ->get();
        return view('beranda.profil', ['kecamatans' => $kecamatan, 'desas' => $desa]);
    }

    public function lihatprofil(Request $request)
    {
        $id_desa =  DB::table('asals')
            ->where('id', $request->desa)
            ->get();
        return view('beranda.profil.viewprofil', ['desas' => $id_desa]);
    }

    public function pilihdesa(Request $request)
    {
        $desas = DB::table('asals')
            ->where('kecamatan', $request->kecamatan)
            ->get();
        return view('beranda.profil.desa', ['desas' => $desas]);
    }
}
