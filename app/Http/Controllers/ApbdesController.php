<?php

namespace App\Http\Controllers;

use App\Models\Apbdes;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use App\Models\Admin;
use Illuminate\Support\Facades\DB;


class ApbdesController extends Controller
{
    /**
     * index
     *
     * @return void
     */
    public function index()
    {
        $Apbdess = Apbdes::all();
        $infos = Admin::where('id', session('loggedAdminDesa'))->first();
        return view('adminDesa.apbdes.index',  [
            'infos' => $infos,
            'apbdes' => $Apbdess
        ]);
    }

    /**
     * create
     *
     * @return void
     */
    public function create()
    {
        return view('adminDesa.apbdes.create');
    }

    /**
     * store
     *
     * @param  mixed $request
     * @return void
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'image'     => 'required|image|mimes:png,jpg,jpeg',
            'title'     => 'required',
            'content'   => 'required'
        ]);

        //upload image
        $image = $request->file('image');
        $image->storeAs('apbdess', $image->hashName());

        $apbdes = apbdes::create([
            'image'     => $image->hashName(),
            'title'     => $request->title,
            'content'   => $request->content
        ]);

        if ($apbdes) {
            //redirect dengan pesan sukses
            return redirect()->route('apbdes.index')->with(['success' => 'Data Berhasil Disimpan!']);
        } else {
            //redirect dengan pesan error
            return redirect()->route('apbdes.index')->with(['error' => 'Data Gagal Disimpan!']);
        }
    }

    /**
     * edit
     *
     * @param  mixed $apbdes
     * @return void
     */
    public function edit(Apbdes $apbdes)
    {
        return view('adminDesa.apbdes.edit', compact('apbdes'));
    }

    /**
     * update
     *
     * @param  mixed $request
     * @param  mixed $apbdes
     * @return void
     */
    public function update(Request $request, Apbdes $apbdes)
    {
        $this->validate($request, [
            'title'     => 'required',
            'content'   => 'required'
        ]);

        //get data apbdes by ID
        $apbdes = Apbdes::findOrFail($apbdes->id);

        if ($request->file('image') == "") {

            $apbdes->update([
                'title'     => $request->title,
                'content'   => $request->content
            ]);
        } else {

            //hapus old image
            Storage::disk('local')->delete('public/apbdess/' . $apbdes->image);

            //upload new image
            $image = $request->file('image');
            $image->storeAs('apbdess', $image->hashName());

            $apbdes->update([
                'image'     => $image->hashName(),
                'title'     => $request->title,
                'content'   => $request->content
            ]);
        }

        if ($apbdes) {
            //redirect dengan pesan sukses
            return redirect()->route('apbdes.index')->with(['success' => 'Data Berhasil Diupdate!']);
        } else {
            //redirect dengan pesan error
            return redirect()->route('apbdes.index')->with(['error' => 'Data Gagal Diupdate!']);
        }
    }

    /**
     * destroy
     *
     * @param  mixed $id
     * @return void
     */
    public function destroy($id)
    {
        $apbdes = Apbdes::findOrFail($id);
        $apbdes->delete();

        if ($apbdes) {
            //redirect dengan pesan sukses
            return redirect()->route('apbdes.index')->with(['success' => 'Data Berhasil Dihapus!']);
        } else {
            //redirect dengan pesan error
            return redirect()->route('apbdes.index')->with(['error' => 'Data Gagal Dihapus!']);
        }
    }
}
